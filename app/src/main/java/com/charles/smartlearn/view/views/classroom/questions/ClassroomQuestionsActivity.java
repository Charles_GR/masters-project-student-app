package com.charles.smartlearn.view.views.classroom.questions;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.classroom.questions.*;
import com.charles.smartlearn.view.templates.AppActivity;
import com.charles.smartlearn.view.views.classroom.ClassroomActivity;

public class ClassroomQuestionsActivity extends AppActivity implements OnClickListener
{

    private static ClassroomQuestionsActivity instance;

    protected ClassroomQuestionsListView classroomQuestionsListView;
    protected ClassroomQuestionAnswerView classroomQuestionAnswerView;
    protected ClassroomQuestionResultsView classroomQuestionResultsView;
    protected ViewFlipper vfQuestions;

    protected Question question;
    protected String questionKey;

    public static ClassroomQuestionsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        setContentView(R.layout.activity_classroom_questions);
        classroomQuestionsListView = new ClassroomQuestionsListView(this);
        classroomQuestionAnswerView = new ClassroomQuestionAnswerView(this);
        classroomQuestionResultsView = new ClassroomQuestionResultsView(this);
        vfQuestions = (ViewFlipper)findViewById(R.id.vfQuestion);
    }

    @Override
    public void onClick(View v)
    {
        ClassroomActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfQuestions.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfQuestions.setDisplayedChild(0);
                break;
            case 2:
                vfQuestions.setDisplayedChild(0);
                break;
            case 3:
                vfQuestions.setDisplayedChild(2);
                break;
        }
    }

}
