package com.charles.smartlearn.view.templates;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.database.students.Course;
import com.charles.smartlearn.model.runnable.ListRunnable;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.CourseData;
import com.charles.smartlearn.processing.database.FirebaseData;
import com.charles.smartlearn.view.views.main.LoginActivity;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase.AuthStateListener;
import com.firebase.client.FirebaseError;
import java.util.List;

public abstract class AppMainActivity extends AppActivity implements OnClickListener, OnLongClickListener, OnItemSelectedListener
{

    protected ProgressBar pbLoadCourses;
    protected Spinner spnSelectedCourse;
    protected TextView tvActionBarTitle;

    private static AuthData previousAuthData;
    private static boolean appInForeground;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FirebaseData.baseRef.addAuthStateListener(authStateListener);
        if(getActionBar() != null && getActionBar().getCustomView() == null)
        {
            getActionBar().setDisplayShowCustomEnabled(true);
            View customView = LayoutInflater.from(this).inflate(R.layout.action_layout_lobby, null);
            customView.findViewById(R.id.ivLogout).setOnClickListener(this);
            customView.findViewById(R.id.ivLogout).setOnLongClickListener(this);
            getActionBar().setCustomView(customView);
            pbLoadCourses = (ProgressBar)getActionBar().getCustomView().findViewById(R.id.pbLoadCourses);
            spnSelectedCourse = (Spinner)getActionBar().getCustomView().findViewById(R.id.spnSelectedCourse);
            tvActionBarTitle = (TextView)getActionBar().getCustomView().findViewById(R.id.tvActionBarTitle);
            spnSelectedCourse.setOnItemSelectedListener(this);
            CourseData.getStudentCourses(getStudentCoursesOnSuccess, getStudentCoursesOnError);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        appInForeground = true;
        checkLoginState();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        appInForeground = false;
    }

    @Override
    public void onClick(View v)
    {
        FirebaseData.baseRef.unauth();
    }

    @Override
    public boolean onLongClick(View v)
    {
        Toast.makeText(this, R.string.logout, Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        Course selectedCourse = (Course)parent.getSelectedItem();
        LearnApplication.getInstance().setSelectedCourse(selectedCourse);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
        LearnApplication.getInstance().setSelectedCourse(null);
    }

    private ListRunnable<Course> getStudentCoursesOnSuccess = new ListRunnable<Course>()
    {
        @Override
        public void run(List<Course> courses)
        {
            ArrayAdapter<Course> courseAdapter = new ArrayAdapter<>(AppMainActivity.this, android.R.layout.simple_spinner_dropdown_item, courses);
            spnSelectedCourse.setAdapter(courseAdapter);
            if(LearnApplication.getInstance().getSelectedCourse() != null)
            {
                spnSelectedCourse.setSelection(courseAdapter.getPosition(LearnApplication.getInstance().getSelectedCourse()));
            }
            pbLoadCourses.setVisibility(View.GONE);
            spnSelectedCourse.setVisibility(View.VISIBLE);
        }
    };

    private SingleArgRunnable<FirebaseError> getStudentCoursesOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            if(FirebaseData.baseRef.getAuth() != null)
            {
                Toast.makeText(AppMainActivity.this, firebaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
            pbLoadCourses.setVisibility(View.GONE);
        }
    };

    private AuthStateListener authStateListener = new AuthStateListener()
    {
        @Override
        public void onAuthStateChanged(AuthData authData)
        {
            if(previousAuthData != null && authData == null && appInForeground)
            {
                Intent intent = new Intent(AppMainActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            previousAuthData = authData;
        }
    };

}
