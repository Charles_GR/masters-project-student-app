package com.charles.smartlearn.view.views.revision.notes;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.adapters.revision.NoteAdapter;
import com.charles.smartlearn.model.database.revision.notes.Note;
import com.charles.smartlearn.model.runnable.*;
import com.charles.smartlearn.processing.database.LoginData;
import com.charles.smartlearn.processing.database.RevisionData;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class RevisionNotesListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private RevisionNotesActivity parent;
    private Button btnCreateNote;
    private Button btnViewNote;
    private Button btnEditNote;
    private Button btnDeleteNote;
    private EditText etNoteSearchQuery;
    private ListView lvNotes;
    private ProgressBar pbLoadNotes;
    private ProgressBar pbViewNote;
    private Spinner spnNoteSortField;
    private Spinner spnNoteSortOrdering;
    private Spinner spnNoteSearchField;
    private TextView tvNotes;
    private TextView tvNoNotesYet;
    private TextView tvNoteListErrorMessage;

    private Note note;

    public RevisionNotesListView(RevisionNotesActivity parent)
    {
        this.parent = parent;
        btnCreateNote = (Button)parent.findViewById(R.id.btnCreateNote);
        btnViewNote = (Button)parent.findViewById(R.id.btnViewNote);
        btnEditNote = (Button)parent.findViewById(R.id.btnEditNote);
        btnDeleteNote = (Button)parent.findViewById(R.id.btnDeleteNote);
        etNoteSearchQuery = (EditText)parent.findViewById(R.id.etNoteSearchQuery);
        lvNotes = (ListView)parent.findViewById(R.id.lvNotes);
        pbLoadNotes = (ProgressBar)parent.findViewById(R.id.pbLoadNotes);
        pbViewNote = (ProgressBar)parent.findViewById(R.id.pbViewNote);
        spnNoteSortField = (Spinner)parent.findViewById(R.id.spnNoteSortField);
        spnNoteSortOrdering = (Spinner)parent.findViewById(R.id.spnNoteSortOrdering);
        spnNoteSearchField = (Spinner)parent.findViewById(R.id.spnNoteSearchField);
        tvNotes = (TextView)parent.findViewById(R.id.tvNotes);
        tvNoNotesYet = (TextView)parent.findViewById(R.id.tvNoNotesYet);
        tvNoteListErrorMessage = (TextView)parent.findViewById(R.id.tvNoteListErrorMessage);
        btnCreateNote.setOnClickListener(this);
        btnViewNote.setOnClickListener(this);
        btnEditNote.setOnClickListener(this);
        btnDeleteNote.setOnClickListener(this);
        etNoteSearchQuery.addTextChangedListener(this);
        spnNoteSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Title", "Author", "Date Created", "# Comments"}));
        spnNoteSortField.setOnItemSelectedListener(this);
        spnNoteSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnNoteSortOrdering.setOnItemSelectedListener(this);
        spnNoteSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Title", "Author"}));
        spnNoteSearchField.setOnItemSelectedListener(this);
        RevisionData.getNotes(getNotesOnSuccess, getNotesOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnCreateNote:
                createNote();
                break;
            case R.id.btnViewNote:
                viewNote();
                break;
            case R.id.btnEditNote:
                editNote();
                break;
            case R.id.btnDeleteNote:
                deleteNote();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortNotes();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortNotes();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortNotes()
    {
        lvNotes.clearChoices();
        NoteAdapter noteAdapter = (NoteAdapter)lvNotes.getAdapter();
        if(noteAdapter != null)
        {
            noteAdapter.searchItems(spnNoteSearchField.getSelectedItem().toString(), etNoteSearchQuery.getText().toString());
            noteAdapter.sortItems(spnNoteSortField.getSelectedItem().toString(), spnNoteSortOrdering.getSelectedItem().toString());
        }
    }

    private Note getSelectedNote()
    {
        Note selectedNote = (Note)lvNotes.getItemAtPosition(lvNotes.getCheckedItemPosition());
        if(selectedNote == null)
        {
            tvNotes.requestFocus();
            tvNotes.setError(parent.getString(R.string.no_note_selected));
        }
        return selectedNote;
    }

    private void createNote()
    {
        tvNotes.setError(null);
        parent.revisionNoteFormView.clearNoteForm();
        parent.currentNoteKey = null;
        parent.vfNotes.setDisplayedChild(3);
    }

    private void viewNote()
    {
        tvNotes.setError(null);
        Note note = getSelectedNote();
        if(note == null)
        {
            return;
        }
        setViewNoteInProgress();
        parent.currentNoteKey = note.getKey();
        parent.revisionNoteContentView.viewNote(viewNoteOnSuccess, viewNoteOnError);
    }

    protected void reloadNote()
    {
        parent.revisionNoteContentView.viewNote(viewNoteOnSuccess, viewNoteOnError);
    }

    private void editNote()
    {
        tvNotes.setError(null);
        note = getSelectedNote();
        if(note != null)
        {
            RevisionData.getNoteImages(getNoteImagesOnSuccess, getNoteImagesOnError);
        }
    }

    private void deleteNote()
    {
        tvNotes.setError(null);
        Note selectedNote = getSelectedNote();
        if(selectedNote == null)
        {
            return;
        }
        if(!selectedNote.getAuthor().equals(LoginData.getUsername()))
        {
            tvNotes.requestFocus();
            tvNotes.setError(parent.getString(R.string.not_note_author));
            return;
        }
        RevisionData.deleteNote(selectedNote.getKey(), deleteNoteOnError);
    }

    private void setViewNoteInProgress()
    {
        pbViewNote.setVisibility(View.VISIBLE);
        btnCreateNote.setEnabled(false);
        btnViewNote.setEnabled(false);
        btnEditNote.setEnabled(false);
        btnDeleteNote.setEnabled(false);
    }

    private void setViewNoteComplete()
    {
        btnCreateNote.setEnabled(true);
        btnViewNote.setEnabled(true);
        btnEditNote.setEnabled(true);
        btnDeleteNote.setEnabled(true);
        pbViewNote.setVisibility(View.GONE);
    }

    private ListRunnable<Note> getNotesOnSuccess = new ListRunnable<Note>()
    {
        @Override
        public void run(List<Note> notes)
        {
            lvNotes.setAdapter(new NoteAdapter(parent, notes));
            tvNoNotesYet.setVisibility(notes.isEmpty() ? View.VISIBLE : View.GONE);
            tvNoteListErrorMessage.setVisibility(View.GONE);
            pbLoadNotes.setVisibility(View.GONE);
            btnCreateNote.setEnabled(true);
            btnViewNote.setEnabled(true);
            btnEditNote.setEnabled(true);
            btnDeleteNote.setEnabled(true);
            searchAndSortNotes();
        }
    };

    private SingleArgRunnable<String> getNotesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvNotes.setAdapter(new NoteAdapter(parent));
            tvNoNotesYet.setVisibility(View.GONE);
            tvNoteListErrorMessage.setText(text);
            tvNoteListErrorMessage.setVisibility(View.VISIBLE);
            pbLoadNotes.setVisibility(View.GONE);
            btnCreateNote.setEnabled(true);
            btnViewNote.setEnabled(true);
            btnEditNote.setEnabled(true);
            btnDeleteNote.setEnabled(true);
        }
    };

    private Runnable viewNoteOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            parent.vfNotes.setDisplayedChild(1);
            setViewNoteComplete();
        }
    };

    private SingleArgRunnable<String> viewNoteOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            setViewNoteComplete();
        }
    };

    private SingleArgRunnable<String> deleteNoteOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvNoteListErrorMessage.setText(text);
            tvNoteListErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private HashMapRunnable<String, String> getNoteImagesOnSuccess = new HashMapRunnable<String, String>()
    {
        @Override
        public void run(HashMap<String, String> images)
        {
            for(Entry<String, String> entry : images.entrySet())
            {
                if(note.getContent().contains(entry.getKey()))
                {
                    LearnApplication.getInstance().getImages().put(entry.getKey(), entry.getValue());
                }
            }
            parent.revisionNoteFormView.editNote(note);
            parent.vfNotes.setDisplayedChild(3);
        }
    };

    private SingleArgRunnable<String> getNoteImagesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvNoteListErrorMessage.setText(text);
            tvNoteListErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
