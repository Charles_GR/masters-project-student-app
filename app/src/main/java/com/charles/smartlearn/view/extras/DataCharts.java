package com.charles.smartlearn.view.extras;

import com.charles.smartlearn.model.LearnApplication;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.util.*;

public abstract class DataCharts
{

    private static final int COLOUR_WHITE = LearnApplication.getInstance().getResources().getColor(android.R.color.white);
    private static final List<Integer> COLOURS = new ArrayList<>();

    static
    {
        for(int colour : ColorTemplate.COLORFUL_COLORS)
        {
            COLOURS.add(colour);
        }
        for(int colour : ColorTemplate.JOYFUL_COLORS)
        {
            COLOURS.add(colour);
        }
        for(int colour : ColorTemplate.VORDIPLOM_COLORS)
        {
            COLOURS.add(colour);
        }
    }

    @SuppressWarnings("unchecked")
    public static void drawBarChart(BarChart barChart, HashMap<String, Integer> answerCounts)
    {
        Collections.shuffle(COLOURS);
        List<String> labels = new ArrayList<>();
        List<IBarDataSet> dataSets = new ArrayList<>();
        int index = 0;
        for(Map.Entry<String, Integer> answerCount : answerCounts.entrySet())
        {
            dataSets.add(createBarDataSet(answerCount, index));
            labels.add(answerCount.getKey());
            index++;
        }
        BarData barData = new BarData(labels, dataSets);
        barData.setValueTextColor(COLOUR_WHITE);
        barData.setValueTextSize(14f);
        barChart.setDescription(null);
        barChart.getXAxis().setDrawLabels(false);
        barChart.getXAxis().setTextColor(COLOUR_WHITE);
        barChart.getAxisLeft().setTextColor(COLOUR_WHITE);
        barChart.getAxisLeft().setAxisMinValue(0);
        barChart.getAxisRight().setDrawLabels(false);
        barChart.getLegend().setEnabled(true);
        barChart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        barChart.getLegend().setTextColor(COLOUR_WHITE);
        barChart.getLegend().setWordWrapEnabled(true);
        barChart.setExtraOffsets(10f, 10f, 10f, 10f);
        barChart.animateXY(1000, 1000);
        barChart.setData(barData);
        barChart.invalidate();
    }

    @SuppressWarnings("unchecked")
    public static void drawPieChart(PieChart pieChart, HashMap<String, Integer> answerCounts)
    {
        Collections.shuffle(COLOURS);
        List<String> labels = new ArrayList<>();
        List<Entry> entries = new ArrayList<>();
        int index = 0;
        for(Map.Entry<String, Integer> answerCount : answerCounts.entrySet())
        {
            entries.add(new Entry(answerCount.getValue(), index));
            labels.add(answerCount.getKey());
            index++;
        }
        PieDataSet pieDataSet = new PieDataSet(entries, null);
        pieDataSet.setColors(COLOURS);
        PieData pieData = new PieData(labels, pieDataSet);
        pieData.setValueTextColor(COLOUR_WHITE);
        pieData.setValueTextSize(14f);
        pieData.setValueFormatter(valueFormatter);
        pieChart.setDescription(null);
        pieChart.setDrawHoleEnabled(false);
        pieChart.setDrawSliceText(false);
        pieChart.getLegend().setEnabled(true);
        pieChart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        pieChart.getLegend().setTextColor(COLOUR_WHITE);
        pieChart.getLegend().setWordWrapEnabled(true);
        pieChart.setExtraOffsets(10f, 10f, 10f, 10f);
        pieChart.animateXY(1000, 1000);
        pieChart.setData(pieData);
        pieChart.invalidate();
    }

    private static BarDataSet createBarDataSet(Map.Entry<String, Integer> answerCount, int index)
    {
        BarEntry barEntry = new BarEntry(answerCount.getValue(), index, answerCount.getKey());
        BarDataSet barDataSet = new BarDataSet(Collections.singletonList(barEntry), answerCount.getKey());
        barDataSet.setColor(COLOURS.get(index % COLOURS.size()));
        barDataSet.setValueFormatter(valueFormatter);
        return barDataSet;
    }

    private static ValueFormatter valueFormatter = new ValueFormatter()
    {
        @Override
        public String getFormattedValue(float v, Entry entry, int i, ViewPortHandler viewPortHandler)
        {
            return String.valueOf(Math.round(v));
        }
    };

}