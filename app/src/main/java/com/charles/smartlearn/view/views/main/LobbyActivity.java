package com.charles.smartlearn.view.views.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.view.templates.AppMainActivity;
import com.charles.smartlearn.view.views.account.AccountActivity;
import com.charles.smartlearn.view.views.classroom.ClassroomActivity;
import com.charles.smartlearn.view.views.performance.PerformanceActivity;
import com.charles.smartlearn.view.views.revision.RevisionActivity;

public class LobbyActivity extends AppMainActivity
{

    private LinearLayout llClassroom;
    private LinearLayout llRevision;
    private LinearLayout llPerformance;
    private LinearLayout llAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);
        llClassroom = (LinearLayout)findViewById(R.id.llClassroom);
        llRevision = (LinearLayout)findViewById(R.id.llRevision);
        llPerformance = (LinearLayout)findViewById(R.id.llPerformance);
        llAccount = (LinearLayout)findViewById(R.id.llAccount);
        llClassroom.setOnClickListener(this);
        llRevision.setOnClickListener(this);
        llPerformance.setOnClickListener(this);
        llAccount.setOnClickListener(this);
        tvActionBarTitle.setText(R.string.lobby_title);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.llClassroom:
                if(courseIsSelected())
                {
                    startActivity(new Intent(this, ClassroomActivity.class));
                }
                break;
            case R.id.llRevision:
                if(courseIsSelected())
                {
                    startActivity(new Intent(this, RevisionActivity.class));
                }
                break;
            case R.id.llPerformance:
                startActivity(new Intent(this, PerformanceActivity.class));
                break;
            case R.id.llAccount:
                startActivity(new Intent(this, AccountActivity.class));
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    private boolean courseIsSelected()
    {
        if(LearnApplication.getInstance().getSelectedCourse() == null)
        {
            Toast.makeText(this, R.string.must_select_course, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

}
