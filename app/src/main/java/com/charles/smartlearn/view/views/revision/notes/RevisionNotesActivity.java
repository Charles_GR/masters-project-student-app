package com.charles.smartlearn.view.views.revision.notes;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.revision.notes.Note;
import com.charles.smartlearn.view.templates.AppActivity;
import com.charles.smartlearn.view.views.revision.RevisionActivity;

public class RevisionNotesActivity extends AppActivity implements OnClickListener
{

    private static RevisionNotesActivity instance;

    protected RevisionNotesListView revisionNotesListView;
    protected RevisionNoteFormView revisionNoteFormView;
    protected RevisionNoteContentView revisionNoteContentView;
    protected ViewFlipper vfNotes;

    protected String currentNoteKey;
    protected String currentNoteContent;

    public static RevisionNotesActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        setContentView(R.layout.activity_revision_notes);
        revisionNotesListView = new RevisionNotesListView(this);
        revisionNoteFormView = new RevisionNoteFormView(this);
        revisionNoteContentView = new RevisionNoteContentView(this);
        vfNotes = (ViewFlipper)findViewById(R.id.vfNotes);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("currentNoteView", vfNotes.getDisplayedChild());
        outState.putString("currentNoteKey", currentNoteKey);
        outState.putString("currentNoteContent", currentNoteContent);
        revisionNoteFormView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        vfNotes.setDisplayedChild(savedInstanceState.getInt("currentNoteView"));
        currentNoteKey = savedInstanceState.getString("currentNoteKey");
        currentNoteContent = savedInstanceState.getString("currentNoteContent");
        switch(vfNotes.getDisplayedChild())
        {
            case 1:
                revisionNotesListView.reloadNote();
                break;
            case 4:
                Note note = new Note(null, savedInstanceState.getString("currentNoteTitle"), currentNoteContent);
                revisionNoteFormView.createNoteViews(note);
                break;
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        revisionNoteFormView.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v)
    {
        RevisionActivity.getInstance().onClick(v);
    }

    public void appendVideo(String videoID)
    {
        revisionNoteFormView.appendVideo(videoID);
    }

    public void onUpButtonPressed()
    {
        switch(vfNotes.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfNotes.setDisplayedChild(0);
                break;
            case 2:
                vfNotes.setDisplayedChild(1);
                break;
            case 3:
                vfNotes.setDisplayedChild(0);
                break;
            case 4:
                vfNotes.setDisplayedChild(3);
                break;
        }
    }

}