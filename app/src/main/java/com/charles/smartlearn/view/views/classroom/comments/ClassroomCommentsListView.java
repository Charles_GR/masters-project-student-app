package com.charles.smartlearn.view.views.classroom.comments;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.adapters.classroom.CommentAdapter;
import com.charles.smartlearn.model.database.classroom.comments.Comment;
import com.charles.smartlearn.model.runnable.ListRunnable;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.*;
import java.util.List;

public class ClassroomCommentsListView implements OnClickListener, OnItemLongClickListener, OnItemSelectedListener, OnGroupExpandListener, OnGroupCollapseListener, TextWatcher
{

    private ClassroomCommentsActivity parent;
    private Button btnAddComment;
    private Button btnDeleteComment;
    private EditText etSearchQuery;
    private ExpandableListView elvComments;
    private ProgressBar pbLoadComments;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvComments;
    private TextView tvNoCommentsYet;
    private TextView tvCommentListErrorMessage;

    private int expandedGroupPos;

    public ClassroomCommentsListView(ClassroomCommentsActivity parent)
    {
        this.parent = parent;
        btnAddComment = (Button)parent.findViewById(R.id.btnAddComment);
        btnDeleteComment = (Button)parent.findViewById(R.id.btnDeleteComment);
        etSearchQuery = (EditText)parent.findViewById(R.id.etSearchQuery);
        elvComments = (ExpandableListView)parent.findViewById(R.id.elvComments);
        pbLoadComments = (ProgressBar)parent.findViewById(R.id.pbLoadComments);
        spnSortField = (Spinner)parent.findViewById(R.id.spnSortField);
        spnSortOrdering = (Spinner)parent.findViewById(R.id.spnSortOrdering);
        spnSearchField = (Spinner)parent.findViewById(R.id.spnSearchField);
        tvComments = (TextView)parent.findViewById(R.id.tvComments);
        tvNoCommentsYet = (TextView)parent.findViewById(R.id.tvNoCommentsYet);
        tvCommentListErrorMessage = (TextView)parent.findViewById(R.id.tvCommentListErrorMessage);
        btnAddComment.setOnClickListener(this);
        btnDeleteComment.setOnClickListener(this);
        etSearchQuery.addTextChangedListener(this);
        elvComments.setOnItemLongClickListener(this);
        elvComments.setOnGroupExpandListener(this);
        elvComments.setOnGroupCollapseListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Comment", "Author", "Date Created", "# Replies"}));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Comment", "Author"}));
        spnSearchField.setOnItemSelectedListener(this);
        expandedGroupPos = -1;
        ClassroomData.getComments(getCommentsOnSuccess, getCommentsOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddComment:
                addComment();
                break;
            case R.id.btnDeleteComment:
                deleteComment();
                break;
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
    {
        ExpandableListView expandableListView = (ExpandableListView)parent;
        expandableListView.setItemChecked(position, !expandableListView.isItemChecked(position));
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortComments();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void onGroupExpand(int groupPosition)
    {
        if(expandedGroupPos > -1 && groupPosition != expandedGroupPos)
        {
            elvComments.collapseGroup(expandedGroupPos);
        }
        expandedGroupPos = groupPosition;
    }

    @Override
    public void onGroupCollapse(int groupPosition)
    {
        expandedGroupPos = -1;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortComments();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortComments()
    {
        elvComments.clearChoices();
        CommentAdapter commentAdapter = (CommentAdapter)elvComments.getExpandableListAdapter();
        if(commentAdapter != null)
        {
            commentAdapter.searchComments(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            commentAdapter.sortComments(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private Comment getSelectedComment()
    {
        elvComments.collapseGroup(elvComments.getCheckedItemPosition());
        Comment selectedComment = (Comment)elvComments.getItemAtPosition(elvComments.getCheckedItemPosition());
        if(selectedComment == null)
        {
            tvComments.requestFocus();
            tvComments.setError(parent.getString(R.string.no_comment_selected));
        }
        return selectedComment;
    }


    private void addComment()
    {
        tvComments.setError(null);
        parent.classroomCommentFormView.clearCommentForm();
        parent.vfComments.setDisplayedChild(1);
    }

    private void deleteComment()
    {
        tvComments.setError(null);
        Comment selectedComment = getSelectedComment();
        if(selectedComment == null)
        {
            return;
        }
        if(!selectedComment.getAuthor().equals(LoginData.getUsername()))
        {
            tvComments.requestFocus();
            tvComments.setError(parent.getString(R.string.not_comment_author));
            return;
        }
        FirebaseData.getClassroomCommentsRef().child(selectedComment.getKey()).removeValue();
    }

    private ListRunnable<Comment> getCommentsOnSuccess = new ListRunnable<Comment>()
    {
        @Override
        public void run(List<Comment> comments)
        {
            elvComments.setAdapter(new CommentAdapter(parent, comments));
            if(expandedGroupPos > -1)
            {
                elvComments.expandGroup(expandedGroupPos);
            }
            tvNoCommentsYet.setVisibility(comments.isEmpty() ? View.VISIBLE : View.GONE);
            tvCommentListErrorMessage.setVisibility(View.GONE);
            pbLoadComments.setVisibility(View.GONE);
            btnAddComment.setEnabled(true);
            btnDeleteComment.setEnabled(true);
            searchAndSortComments();
        }
    };

    private SingleArgRunnable<String> getCommentsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            elvComments.setAdapter(new CommentAdapter(parent));
            tvNoCommentsYet.setVisibility(View.GONE);
            tvCommentListErrorMessage.setText(text);
            tvCommentListErrorMessage.setVisibility(View.VISIBLE);
            pbLoadComments.setVisibility(View.GONE);
            btnAddComment.setEnabled(true);
            btnDeleteComment.setEnabled(true);
        }
    };

}
