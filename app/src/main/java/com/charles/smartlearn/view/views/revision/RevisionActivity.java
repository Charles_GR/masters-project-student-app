package com.charles.smartlearn.view.views.revision;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.view.templates.TabbedActivity;
import com.charles.smartlearn.view.views.revision.notes.RevisionNotesActivity;
import com.charles.smartlearn.view.views.revision.quizzes.RevisionQuizzesActivity;

public class RevisionActivity extends TabbedActivity
{

    private static RevisionActivity instance;

    public static RevisionActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_revision);
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        tvActionBarTitle.setText(R.string.revision_title);
        tvActionBarCourse.setText(LearnApplication.getInstance().getSelectedCourse().getCode());
        tvActionBarCourse.setVisibility(View.VISIBLE);
        createTab(getString(R.string.notes_title), RevisionNotesActivity.class);
        createTab(getString(R.string.quizzes_title), RevisionQuizzesActivity.class);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("currentTab", thTabbedView.getCurrentTab());
        if(thTabbedView.getCurrentTab() == 0)
        {
            RevisionNotesActivity.getInstance().onSaveInstanceState(outState);
        }
        else if(thTabbedView.getCurrentTab() == 1)
        {
            RevisionQuizzesActivity.getInstance().onSaveInstanceState(outState);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        thTabbedView.setCurrentTab(savedInstanceState.getInt("currentTab"));
        if(thTabbedView.getCurrentTab() == 0)
        {
            RevisionNotesActivity.getInstance().onRestoreInstanceState(savedInstanceState);
        }
        else if(thTabbedView.getCurrentTab() == 1)
        {
            RevisionQuizzesActivity.getInstance().onSaveInstanceState(savedInstanceState);
        }
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        RevisionNotesActivity.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onUpButtonPressed()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                RevisionNotesActivity.getInstance().onUpButtonPressed();
                break;
            case 1:
                RevisionQuizzesActivity.getInstance().onUpButtonPressed();
                break;
        }
    }

}
