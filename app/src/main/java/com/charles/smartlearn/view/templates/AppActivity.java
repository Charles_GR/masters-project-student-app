package com.charles.smartlearn.view.templates;

import android.app.Activity;
import android.content.Intent;
import android.view.inputmethod.InputMethodManager;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.FirebaseData;
import com.charles.smartlearn.processing.database.LoginData;
import com.charles.smartlearn.view.views.main.LobbyActivity;
import com.firebase.client.AuthData;

public abstract class AppActivity extends Activity
{

    public void goToLobby()
    {
        startActivity(new Intent(this, LobbyActivity.class));
    }

    public void hideKeyboard()
    {
        if(getCurrentFocus() != null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public boolean checkCourseIsSelected()
    {
        if(LearnApplication.getInstance().getSelectedCourse() == null)
        {
            goToLobby();
            return false;
        }
        return true;
    }

    protected void checkLoginState()
    {
        AuthData authData = FirebaseData.baseRef.getAuth();
        if(authData == null)
        {
            finish();
        }
        else
        {
            LoginData.setEmail(authData.getProviderData().get("email").toString());
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
        }
    }

    private SingleArgRunnable<String> checkLoginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setUsername(text);
        }
    };

    private SingleArgRunnable<String> checkLoginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            FirebaseData.baseRef.unauth();
        }
    };

}