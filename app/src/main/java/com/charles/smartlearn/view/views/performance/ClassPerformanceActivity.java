package com.charles.smartlearn.view.views.performance;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.performance.Performance;
import com.charles.smartlearn.model.database.students.Course;
import com.charles.smartlearn.model.runnable.*;
import com.charles.smartlearn.processing.database.*;
import com.charles.smartlearn.view.templates.AppActivity;
import java.util.*;

public class ClassPerformanceActivity extends AppActivity implements OnItemSelectedListener
{

    private Spinner spnTimePeriod;
    private Spinner spnCourse;
    private TextView tvBestStudents;
    private TextView tvAverageScore;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_performance);
        spnTimePeriod = (Spinner)findViewById(R.id.spnTimePeriod);
        spnCourse = (Spinner)findViewById(R.id.spnCourse);
        tvBestStudents = (TextView)findViewById(R.id.tvBestStudentsValue);
        tvAverageScore = (TextView)findViewById(R.id.tvAverageScoreValue);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        spnTimePeriod.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new String[]{"All Time", "Past Year", "Past 6 Months", "Past 3 Months", "Past Month", "Past 2 Weeks", "Past Week", "Past 3 Days", "Past Day"}));
        spnTimePeriod.setOnItemSelectedListener(this);
        spnCourse.setOnItemSelectedListener(this);
        CourseData.getCourses(getCoursesOnSuccess, getCoursesOnError);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch(parent.getId())
        {
            case R.id.spnTimePeriod:
                if(spnCourse.getSelectedItem() != null)
                {
                    PerformanceData.getCoursePerformances(spnCourse.getSelectedItem().toString(), spnTimePeriod.getSelectedItem().toString(), getPerformancesOnSuccess, getPerformancesOnError);
                }
                break;
            case R.id.spnCourse:
                if(spnTimePeriod.getSelectedItem() != null)
                {
                    PerformanceData.getCoursePerformances(spnCourse.getSelectedItem().toString(), spnTimePeriod.getSelectedItem().toString(), getPerformancesOnSuccess, getPerformancesOnError);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    private static String writeBestStudents(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> bestStudents = Performance.getBestPerformances(performances);
        return Performance.performancesToString(bestStudents);
    }

    private ListRunnable<Course> getCoursesOnSuccess = new ListRunnable<Course>()
    {
        @Override
        public void run(List<Course> courses)
        {
            spnCourse.setAdapter(new ArrayAdapter<>(ClassPerformanceActivity.this, android.R.layout.simple_spinner_item, courses));
        }
    };

    private SingleArgRunnable<String> getCoursesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private HashMapRunnable<String, Performance> getPerformancesOnSuccess = new HashMapRunnable<String, Performance>()
    {
        @Override
        public void run(HashMap<String, Performance> performances)
        {
            tvBestStudents.setText(writeBestStudents(performances));
            tvAverageScore.setText(performances.isEmpty() ? getString(R.string.not_yet_available) : Performance.writeAverageScore(performances));
        }
    };

    private SingleArgRunnable<String> getPerformancesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
