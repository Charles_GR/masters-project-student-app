package com.charles.smartlearn.view.views.revision.quizzes;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.quiz.QuestionResult;
import com.charles.smartlearn.model.quiz.QuizMaster;

public class RevisionQuizFeedbackView implements OnClickListener
{

    private RevisionQuizzesActivity parent;
    private Button btnPreviousQuestionA;
    private Button btnNextQuestionA;
    private TextView tvQuizScore;
    private TextView tvNoFeedbackAvailable;
    private ViewFlipper vfFeedback;

    public RevisionQuizFeedbackView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        btnPreviousQuestionA = (Button)parent.findViewById(R.id.btnPreviousQuestionA);
        btnNextQuestionA = (Button)parent.findViewById(R.id.btnNextQuestionA);
        tvQuizScore = (TextView)parent.findViewById(R.id.tvQuizScore);
        tvNoFeedbackAvailable = (TextView)parent.findViewById(R.id.tvNoFeedbackAvailable);
        vfFeedback = (ViewFlipper)parent.findViewById(R.id.vfFeedback);
        btnPreviousQuestionA.setOnClickListener(this);
        btnNextQuestionA.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnPreviousQuestionA:
                vfFeedback.setDisplayedChild(vfFeedback.getDisplayedChild() - 1);
                btnPreviousQuestionA.setEnabled(vfFeedback.getDisplayedChild() > 0);
                btnNextQuestionA.setEnabled(vfFeedback.getDisplayedChild() < vfFeedback.getChildCount() - 1);
                break;
            case R.id.btnNextQuestionA:
                vfFeedback.setDisplayedChild(vfFeedback.getDisplayedChild() + 1);
                btnPreviousQuestionA.setEnabled(vfFeedback.getDisplayedChild() > 0);
                btnNextQuestionA.setEnabled(vfFeedback.getDisplayedChild() < vfFeedback.getChildCount() - 1);
                break;
        }
    }

    protected void displayResults()
    {
        vfFeedback.removeAllViews();
        tvQuizScore.setText(parent.getString(R.string.quiz_score, QuizMaster.getInstance().getPointsAwarded(), QuizMaster.getInstance().getPointsPossible(), QuizMaster.getInstance().getPercentageScore()));
        tvQuizScore.append(parent.getString(R.string.below_is_some_feedback));
        for(QuestionResult questionResult : QuizMaster.getInstance().getQuestionResults().values())
        {
            if(questionResult == null || questionResult.isEmpty())
            {
                continue;
            }
            TextView tvQuestionResult = new TextView(parent);
            tvQuestionResult.setSingleLine(false);
            tvQuestionResult.setGravity(Gravity.CENTER_HORIZONTAL);
            tvQuestionResult.setText(questionResult.writeFeedback());
            vfFeedback.addView(tvQuestionResult);
        }
        tvNoFeedbackAvailable.setVisibility(vfFeedback.getChildCount() == 0 ? View.VISIBLE : View.GONE);
        btnPreviousQuestionA.setEnabled(vfFeedback.getDisplayedChild() > 0);
        btnNextQuestionA.setEnabled(vfFeedback.getDisplayedChild() < vfFeedback.getChildCount() - 1);
    }

}
