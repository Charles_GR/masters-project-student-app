package com.charles.smartlearn.view.views.revision.notes;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.adapters.revision.NoteCommentAdapter;
import com.charles.smartlearn.model.database.revision.notes.Comment;
import com.charles.smartlearn.model.runnable.ListRunnable;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.*;
import com.charles.smartlearn.view.extras.PopupDialogs;
import java.util.List;

public class RevisionNoteContentView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private RevisionNotesActivity parent;
    private Button btnViewComments;
    private Button btnAddComment;
    private Button btnDeleteComment;
    private EditText etCommentSearchQuery;
    private LinearLayout llNoteContent;
    private ListView lvComments;
    private ProgressBar pbLoadNoteComments;
    private Spinner spnCommentSortField;
    private Spinner spnCommentSortOrdering;
    private Spinner spnCommentSearchField;
    private TextView tvNoteTitle;
    private TextView tvComments;
    private TextView tvNoCommentsYet;
    private TextView tvNoteCommentsListErrorMessage;

    public RevisionNoteContentView(RevisionNotesActivity parent)
    {
        this.parent = parent;
        btnViewComments = (Button)parent.findViewById(R.id.btnViewComments);
        btnAddComment = (Button)parent.findViewById(R.id.btnAddComment);
        btnDeleteComment = (Button)parent.findViewById(R.id.btnDeleteComment);
        etCommentSearchQuery = (EditText)parent.findViewById(R.id.etCommentSearchQuery);
        llNoteContent = (LinearLayout)parent.findViewById(R.id.llNoteContent);
        lvComments = (ListView)parent.findViewById(R.id.lvComments);
        pbLoadNoteComments = (ProgressBar)parent.findViewById(R.id.pbLoadNoteComments);
        spnCommentSortField = (Spinner)parent.findViewById(R.id.spnCommentSortField);
        spnCommentSortOrdering = (Spinner)parent.findViewById(R.id.spnCommentSortOrdering);
        spnCommentSearchField = (Spinner)parent.findViewById(R.id.spnCommentSearchField);
        tvNoteTitle = (TextView)parent.findViewById(R.id.tvNoteTitle);
        tvComments = (TextView)parent.findViewById(R.id.tvComments);
        tvNoCommentsYet = (TextView)parent.findViewById(R.id.tvNoCommentsYet);
        tvNoteCommentsListErrorMessage = (TextView)parent.findViewById(R.id.tvNoteCommentsListErrorMessage);
        btnViewComments.setOnClickListener(this);
        btnAddComment.setOnClickListener(this);
        btnDeleteComment.setOnClickListener(this);
        etCommentSearchQuery.addTextChangedListener(this);
        spnCommentSortField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Comment", "Author", "Date Created"}));
        spnCommentSortField.setSelection(2);
        spnCommentSortField.setOnItemSelectedListener(this);
        spnCommentSortOrdering.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Ascending", "Descending"}));
        spnCommentSortOrdering.setSelection(1);
        spnCommentSortOrdering.setOnItemSelectedListener(this);
        spnCommentSearchField.setAdapter(new ArrayAdapter<>(parent, R.layout.spinner_item, new String[]{"Comment", "Author"}));
        spnCommentSearchField.setOnItemSelectedListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnViewComments:
                viewComments();
                break;
            case R.id.btnAddComment:
                addComment();
                break;
            case R.id.btnDeleteComment:
                deleteComment();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortComments();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortComments();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    protected void viewNote(Runnable viewNoteOnSuccess, SingleArgRunnable<String> viewNoteOnError)
    {
        RevisionData.viewNote(parent, parent.currentNoteKey, tvNoteTitle, llNoteContent, viewNoteOnSuccess, viewNoteOnError);
    }

    private Comment getSelectedComment()
    {
        Comment selectedComment = (Comment)lvComments.getItemAtPosition(lvComments.getCheckedItemPosition());
        if(selectedComment == null)
        {
            tvComments.requestFocus();
            tvComments.setError(parent.getString(R.string.no_comment_selected));
        }
        return selectedComment;
    }

    private void searchAndSortComments()
    {
        lvComments.clearChoices();
        NoteCommentAdapter commentAdapter = (NoteCommentAdapter)lvComments.getAdapter();
        if(commentAdapter != null)
        {
            commentAdapter.searchItems(spnCommentSearchField.getSelectedItem().toString(), etCommentSearchQuery.getText().toString());
            commentAdapter.sortItems(spnCommentSortField.getSelectedItem().toString(), spnCommentSortOrdering.getSelectedItem().toString());
        }
    }

    private void viewComments()
    {
        parent.vfNotes.setDisplayedChild(2);
        RevisionData.getNoteComments(parent.currentNoteKey, getNoteCommentsOnSuccess, getNoteCommentsOnError);
    }

    private void addComment()
    {
        tvComments.setError(null);
        PopupDialogs.showInputDialog(parent, "Add Comment", "", "Post", "Cancel", onCommentInputEntered);
    }

    private void deleteComment()
    {
        tvComments.setError(null);
        Comment selectedComment = getSelectedComment();
        if(selectedComment == null)
        {
            return;
        }
        if(!selectedComment.getAuthor().equals(LoginData.getUsername()))
        {
            tvComments.requestFocus();
            tvComments.setError(parent.getString(R.string.not_comment_author));
            return;
        }
        FirebaseData.getRevisionNoteCommentsRef(parent.currentNoteKey).child(selectedComment.getKey()).removeValue();
    }

    private SingleArgRunnable<String> onCommentInputEntered = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            if(text.isEmpty())
            {
                Toast.makeText(parent, R.string.comment_empty, Toast.LENGTH_SHORT).show();
            }
            else
            {
                RevisionData.postNoteComment(parent.currentNoteKey, text);
            }
        }
    };

    private ListRunnable<Comment> getNoteCommentsOnSuccess = new ListRunnable<Comment>()
    {
        @Override
        public void run(List<Comment> comments)
        {
            lvComments.setAdapter(new NoteCommentAdapter(parent, comments));
            tvNoCommentsYet.setVisibility(comments.isEmpty() ? View.VISIBLE : View.GONE);
            pbLoadNoteComments.setVisibility(View.GONE);
            tvNoteCommentsListErrorMessage.setVisibility(View.GONE);
            btnAddComment.setEnabled(true);
            btnDeleteComment.setEnabled(true);
            searchAndSortComments();
        }
    };

    private SingleArgRunnable<String> getNoteCommentsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvComments.setAdapter(new NoteCommentAdapter(parent));
            tvNoCommentsYet.setVisibility(View.GONE);
            pbLoadNoteComments.setVisibility(View.GONE);
            tvNoteCommentsListErrorMessage.setText(text);
            tvNoteCommentsListErrorMessage.setVisibility(View.VISIBLE);
            btnAddComment.setEnabled(true);
            btnDeleteComment.setEnabled(true);
        }
    };

}

