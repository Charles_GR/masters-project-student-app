package com.charles.smartlearn.view.views.account;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.AccountData;
import com.charles.smartlearn.processing.database.LoginData;
import com.charles.smartlearn.view.templates.AppNavActivity;

public class AccountActivity extends AppNavActivity
{

    private Button btnSaveEmail;
    private Button btnSavePassword;
    private EditText etCurrentPassword;
    private EditText etEmail;
    private EditText etNewPassword;
    private EditText etConfirmNewPassword;
    private ProgressBar pbProgress;
    private TextView tvSuccessMessage;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        btnSaveEmail = (Button)findViewById(R.id.btnSaveEmail);
        btnSavePassword = (Button)findViewById(R.id.btnSavePassword);
        etCurrentPassword = (EditText)findViewById(R.id.etCurrentPassword);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etNewPassword = (EditText)findViewById(R.id.etNewPassword);
        etConfirmNewPassword = (EditText)findViewById(R.id.etConfirmNewPassword);
        pbProgress = (ProgressBar)findViewById(R.id.pbProgress);
        tvSuccessMessage = (TextView)findViewById(R.id.tvSuccessMessage);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        btnSaveEmail.setOnClickListener(this);
        btnSavePassword.setOnClickListener(this);
        etEmail.setText(LoginData.getEmail());
        tvActionBarTitle.setText(R.string.account_title);
        tvActionBarCourse.setVisibility(View.GONE);
    }

    @Override
    protected void onUpButtonPressed()
    {
        goToLobby();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnSaveEmail:
                changeEmail();
                break;
            case R.id.btnSavePassword:
                changePassword();
                break;
            default:
                super.onClick(v);
                break;
        }
    }

    private void changeEmail()
    {
        setActionInProgress();
        if(!validChangeEmailInputs())
        {
            setActionComplete();
            return;
        }
        AccountData.changeEmail(LoginData.getEmail(), etCurrentPassword.getText().toString(),
                etEmail.getText().toString(), changeEmailOnSuccess, changeEmailOnError);
    }

    private void changePassword()
    {
        setActionInProgress();
        if(!validChangePasswordInputs())
        {
            setActionComplete();
            return;
        }
        AccountData.changePassword(etEmail.getText().toString(), etCurrentPassword.getText().toString(),
                etNewPassword.getText().toString(), changePasswordOnSuccess, changePasswordOnError);
    }

    private boolean validChangeEmailInputs()
    {
        if(etCurrentPassword.getText().toString().isEmpty())
        {
            etCurrentPassword.requestFocus();
            etCurrentPassword.setError(getString(R.string.current_password_empty));
            return false;
        }
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        if(etEmail.getText().toString().equals(LoginData.getEmail()))
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_unchanged));
            return false;
        }
        return true;
    }

    private boolean validChangePasswordInputs()
    {
        if(etCurrentPassword.getText().toString().isEmpty())
        {
            etCurrentPassword.requestFocus();
            etCurrentPassword.setError(getString(R.string.current_password_empty));
            return false;
        }
        if(etNewPassword.getText().toString().isEmpty())
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_empty));
            return false;
        }
        if(etConfirmNewPassword.getText().toString().isEmpty())
        {
            etConfirmNewPassword.requestFocus();
            etConfirmNewPassword.setText(getString(R.string.confirm_new_password_empty));
            return false;
        }
        if(!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString()))
        {
            etNewPassword.requestFocus();
            etNewPassword.setText(getString(R.string.new_password_not_match));
            etConfirmNewPassword.setText(getString(R.string.new_password_not_match));
            return false;
        }
        if(etCurrentPassword.getText().toString().equals(etNewPassword.getText().toString()))
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_same_as_current));
            etConfirmNewPassword.setError(getString(R.string.new_password_same_as_current));
            return false;
        }
        if(etNewPassword.getText().toString().length() < 6)
        {
            etNewPassword.requestFocus();
            etNewPassword.setError(getString(R.string.new_password_too_short));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        hideKeyboard();
        clearMessages();
        btnSaveEmail.setEnabled(false);
        btnSavePassword.setEnabled(false);
        pbProgress.setVisibility(View.VISIBLE);
    }

    private void setActionComplete()
    {
        btnSaveEmail.setEnabled(true);
        btnSavePassword.setEnabled(true);
        pbProgress.setVisibility(View.GONE);
    }

    private void clearMessages()
    {
        etCurrentPassword.setError(null);
        etEmail.setError(null);
        etNewPassword.setError(null);
        etConfirmNewPassword.setError(null);
        tvErrorMessage.setText("");
        tvSuccessMessage.setText("");
    }

    private Runnable changeEmailOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            tvSuccessMessage.setText(R.string.email_changed);
            setActionComplete();
        }
    };

    private SingleArgRunnable<String> changeEmailOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            setActionComplete();
        }
    };

    private Runnable changePasswordOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            etCurrentPassword.setText("");
            etNewPassword.setText("");
            etConfirmNewPassword.setText("");
            tvSuccessMessage.setText(R.string.password_changed);
            setActionComplete();
        }
    };

    private SingleArgRunnable<String> changePasswordOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            tvErrorMessage.setText(message);
            setActionComplete();
        }
    };

}