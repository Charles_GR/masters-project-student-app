package com.charles.smartlearn.view.templates;

import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import com.charles.smartlearn.R;

@SuppressWarnings("deprecation")
public abstract class TabbedActivity extends AppNavActivity
{

    protected TabHost thTabbedView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        thTabbedView = (TabHost)findViewById(R.id.thTabbedView);
        LocalActivityManager localActivityManager = new LocalActivityManager(this, false);
        thTabbedView.setup(localActivityManager);
        localActivityManager.dispatchCreate(savedInstanceState);
    }

    protected void createTab(String tag, Class<? extends Activity> activity)
    {
        TabHost.TabSpec spec = thTabbedView.newTabSpec(tag);
        spec.setIndicator(tag);
        spec.setContent(new Intent(this, activity));
        thTabbedView.addTab(spec);
    }

}