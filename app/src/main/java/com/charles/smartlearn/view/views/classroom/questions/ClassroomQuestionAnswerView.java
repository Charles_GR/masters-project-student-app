package com.charles.smartlearn.view.views.classroom.questions;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.classroom.questions.Question;
import com.charles.smartlearn.model.database.classroom.questions.Response;
import com.charles.smartlearn.model.database.performance.Score.ContentType;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.*;

public class ClassroomQuestionAnswerView implements OnClickListener
{

    private ClassroomQuestionsActivity parent;
    private Button btnPostAnswer;
    private LinearLayout llSelectAnswer;
    private TextView tvQuestionA;
    private TextView tvPostAnswerErrorMessage;

    public ClassroomQuestionAnswerView(ClassroomQuestionsActivity parent)
    {
        this.parent = parent;
        btnPostAnswer = (Button)parent.findViewById(R.id.btnPostAnswer);
        tvQuestionA = (TextView)parent.findViewById(R.id.tvQuestionA);
        llSelectAnswer = (LinearLayout)parent.findViewById(R.id.llSelectAnswer);
        tvPostAnswerErrorMessage = (TextView)parent.findViewById(R.id.tvPostAnswerErrorMessage);
        btnPostAnswer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnPostAnswer)
        {
            postAnswer();
        }
    }

    protected void displayQuestion()
    {
        tvQuestionA.setError(null);
        tvQuestionA.setText(parent.question.getQuestion());
        llSelectAnswer.removeAllViews();
        for(Object obj : parent.question.createAnswerViews(parent))
        {
            llSelectAnswer.addView((View)obj);
        }
    }

    private void postAnswer()
    {
        parent.hideKeyboard();
        tvQuestionA.setError(null);
        tvPostAnswerErrorMessage.setText("");
        tvPostAnswerErrorMessage.setVisibility(View.GONE);
        Object answers = parent.question.findStudentAnswers(parent, llSelectAnswer);
        if(answers == null)
        {
            tvQuestionA.requestFocus();
            tvQuestionA.setError(parent.getString(R.string.no_answers_given));
            return;
        }
        Response response = new Response<>(LoginData.getUsername(), answers);
        parent.questionKey = parent.question.getKey();
        ClassroomData.postAnswer(parent.questionKey, response, postAnswerOnSuccess, postAnswerOnError);
        PerformanceData.updatePerformance(ContentType.CLASSROOM_QUESTION_TYPE, parent.questionKey, 1, parent.question.calcPointsForAnswers(llSelectAnswer), parent.question.calcPointsPossible());
    }

    private SingleArgRunnable<Question> postAnswerOnSuccess = new SingleArgRunnable<Question>()
    {
        @Override
        public void run(Question question)
        {
            parent.classroomQuestionResultsView.updateResults();
            parent.vfQuestions.setDisplayedChild(2);
        }
    };

    private SingleArgRunnable<String> postAnswerOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvPostAnswerErrorMessage.setText(text);
            tvPostAnswerErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
