package com.charles.smartlearn.view.views.classroom.questions;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.classroom.questions.Question;
import com.charles.smartlearn.view.extras.DataCharts;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import java.util.HashMap;
import java.util.List;

public class ClassroomQuestionResultsView implements OnClickListener, OnCheckedChangeListener
{

    private ClassroomQuestionsActivity parent;
    private BarChart bcResults;
    private Button btnViewChart;
    private PieChart pcResults;
    private Switch swChartType;
    private TextView tvQuestionB;
    private TextView tvResults;
    private ViewFlipper vfDataCharts;

    public ClassroomQuestionResultsView(ClassroomQuestionsActivity parent)
    {
        this.parent = parent;
        bcResults = (BarChart)parent.findViewById(R.id.bcResults);
        btnViewChart = (Button)parent.findViewById(R.id.btnViewChart);
        pcResults = (PieChart)parent.findViewById(R.id.pcResults);
        swChartType = (Switch)parent.findViewById(R.id.swChartType);
        tvQuestionB = (TextView)parent.findViewById(R.id.tvQuestionB);
        tvResults = (TextView)parent.findViewById(R.id.tvResults);
        vfDataCharts = (ViewFlipper)parent.findViewById(R.id.vfDataCharts);
        btnViewChart.setOnClickListener(this);
        swChartType.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnViewChart)
        {
            viewChart();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        vfDataCharts.setDisplayedChild(swChartType.isChecked() ? 1 : 0);
    }

    @SuppressWarnings("unchecked")
    private void viewChart()
    {
        HashMap<String, Integer> answerCounts = parent.question.calcAnswerCounts();
        if(answerCounts.isEmpty())
        {
            Toast.makeText(parent, R.string.no_chart_data_available, Toast.LENGTH_SHORT).show();
        }
        else
        {
            parent.vfQuestions.setDisplayedChild(3);
            DataCharts.drawBarChart(bcResults, answerCounts);
            DataCharts.drawPieChart(pcResults, answerCounts);
        }
    }

    protected void updateResults()
    {
        tvQuestionB.setText(parent.question.getQuestion());
        tvResults.setText(parent.question.writeResults());
    }

    protected void displayResults(List<Question> questions)
    {
        if(parent.question == null)
        {
            return;
        }
        for(Question question : questions)
        {
            if(question.getKey().equals(parent.question.getKey()))
            {
                displayResults(question);
                break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected void displayResults(Question question)
    {
        parent.question = question;
        tvQuestionB.setText(question.getQuestion());
        tvResults.setText(question.writeResults());
        HashMap<String, Integer> answerCounts = question.calcAnswerCounts();
        DataCharts.drawBarChart(bcResults, answerCounts);
        DataCharts.drawPieChart(pcResults, answerCounts);
    }

}
