package com.charles.smartlearn.view.views.classroom.questions;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.adapters.classroom.QuestionAdapter;
import com.charles.smartlearn.model.database.classroom.questions.Question;
import com.charles.smartlearn.model.runnable.ListRunnable;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.ClassroomData;
import com.charles.smartlearn.processing.database.LoginData;
import java.util.List;

public class ClassroomQuestionsListView implements OnClickListener, OnItemSelectedListener, TextWatcher
{

    private ClassroomQuestionsActivity parent;
    private Button btnAnswerQuestion;
    private Button btnViewQuestion;
    private EditText etSearchQuery;
    private ListView lvQuestions;
    private ProgressBar pbLoadQuestions;
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private Spinner spnSearchField;
    private TextView tvQuestions;
    private TextView tvNoQuestionsYet;
    private TextView tvQuestionListErrorMessage;

    public ClassroomQuestionsListView(ClassroomQuestionsActivity parent)
    {
        this.parent = parent;
        btnAnswerQuestion = (Button)parent.findViewById(R.id.btnAnswerQuestion);
        btnViewQuestion = (Button)parent.findViewById(R.id.btnViewQuestion);
        etSearchQuery = (EditText)parent.findViewById(R.id.etSearchQuery);
        lvQuestions = (ListView)parent.findViewById(R.id.lvQuestions);
        pbLoadQuestions = (ProgressBar)parent.findViewById(R.id.pbLoadQuestions);
        spnSortField = (Spinner)parent.findViewById(R.id.spnSortField);
        spnSortOrdering = (Spinner)parent.findViewById(R.id.spnSortOrdering);
        spnSearchField = (Spinner)parent.findViewById(R.id.spnSearchField);
        tvQuestions = (TextView)parent.findViewById(R.id.tvQuestions);
        tvNoQuestionsYet = (TextView)parent.findViewById(R.id.tvNoQuestionsYet);
        tvQuestionListErrorMessage = (TextView)parent.findViewById(R.id.tvQuestionListErrorMessage);
        btnAnswerQuestion.setOnClickListener(this);
        btnViewQuestion.setOnClickListener(this);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(parent, android.R.layout.simple_spinner_item, new String[]{"Question", "Date Created", "Status"}));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(parent, android.R.layout.simple_spinner_item, new String[]{"Ascending", "Descending"}));
        spnSortOrdering.setOnItemSelectedListener(this);
        spnSearchField.setAdapter(new ArrayAdapter<>(parent, android.R.layout.simple_spinner_item, new String[]{"Question", "Status"}));
        spnSearchField.setOnItemSelectedListener(this);
        ClassroomData.getQuestions(getQuestionsOnSuccess, getQuestionsOnError);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAnswerQuestion:
                answerQuestion();
                break;
            case R.id.btnViewQuestion:
                viewQuestion();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        searchAndSortQuestions();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
        searchAndSortQuestions();
    }

    @Override
    public void afterTextChanged(Editable s)
    {

    }

    private void searchAndSortQuestions()
    {
        lvQuestions.clearChoices();
        QuestionAdapter questionAdapter = (QuestionAdapter)lvQuestions.getAdapter();
        if(questionAdapter != null)
        {
            questionAdapter.searchItems(spnSearchField.getSelectedItem().toString(), etSearchQuery.getText().toString());
            questionAdapter.sortItems(spnSortField.getSelectedItem().toString(), spnSortOrdering.getSelectedItem().toString());
        }
    }

    private Question getSelectedQuestion()
    {
        Question selectedQuestion = (Question)lvQuestions.getItemAtPosition(lvQuestions.getCheckedItemPosition());
        if(selectedQuestion == null)
        {
            tvQuestions.requestFocus();
            tvQuestions.setError(parent.getString(R.string.no_question_selected));
        }
        return selectedQuestion;
    }

    private void answerQuestion()
    {
        tvQuestions.setError(null);
        parent.question = getSelectedQuestion();
        if(parent.question == null)
        {
            return;
        }
        if(parent.question.getResponses().containsKey(LoginData.getUsername()))
        {
            tvQuestions.requestFocus();
            tvQuestions.setError(parent.getString(R.string.question_already_answered));
            return;
        }
        parent.classroomQuestionAnswerView.displayQuestion();
        parent.vfQuestions.setDisplayedChild(1);
    }

    private void viewQuestion()
    {
        tvQuestions.setError(null);
        parent.question = getSelectedQuestion();
        if(parent.question == null)
        {
            return;
        }
        if(!parent.question.getResponses().containsKey(LoginData.getUsername()))
        {
            tvQuestions.requestFocus();
            tvQuestions.setError(parent.getString(R.string.question_not_yet_answered));
            return;
        }
        parent.classroomQuestionResultsView.displayResults(parent.question);
        parent.vfQuestions.setDisplayedChild(2);
    }

    private ListRunnable<Question> getQuestionsOnSuccess = new ListRunnable<Question>()
    {
        @Override
        public void run(List<Question> questions)
        {
            lvQuestions.setAdapter(new QuestionAdapter(parent, questions));
            tvNoQuestionsYet.setVisibility(questions.isEmpty() ? View.VISIBLE : View.GONE);
            pbLoadQuestions.setVisibility(View.GONE);
            parent.classroomQuestionResultsView.displayResults(questions);
            searchAndSortQuestions();
        }
    };

    private SingleArgRunnable<String> getQuestionsOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            lvQuestions.setAdapter(new QuestionAdapter(parent));
            tvNoQuestionsYet.setVisibility(View.GONE);
            pbLoadQuestions.setVisibility(View.GONE);
            tvQuestionListErrorMessage.setText(text);
            tvQuestionListErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
