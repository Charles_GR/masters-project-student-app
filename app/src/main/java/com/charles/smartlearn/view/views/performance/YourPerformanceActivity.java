package com.charles.smartlearn.view.views.performance;

import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.performance.Performance;
import com.charles.smartlearn.model.database.students.Course;
import com.charles.smartlearn.model.runnable.*;
import com.charles.smartlearn.processing.database.*;
import com.charles.smartlearn.view.templates.AppActivity;
import com.firebase.client.FirebaseError;
import java.util.*;

public class YourPerformanceActivity extends AppActivity implements OnItemSelectedListener, OnCheckedChangeListener
{

    private CheckBox chkSharePerformanceData;
    private Spinner spnTimePeriod;
    private Spinner spnCourse;
    private TextView tvBestCourses;
    private TextView tvWorstCourses;
    private TextView tvYourScore;
    private TextView tvAverageScore;
    private TextView tvErrorMessage;

    private Course selectedCourse;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_performance);
        chkSharePerformanceData = (CheckBox)findViewById(R.id.chkSharePerformanceData);
        spnTimePeriod = (Spinner)findViewById(R.id.spnTimePeriod);
        spnCourse = (Spinner)findViewById(R.id.spnCourse);
        tvBestCourses = (TextView)findViewById(R.id.tvBestCoursesValue);
        tvWorstCourses = (TextView)findViewById(R.id.tvWorstCoursesValue);
        tvYourScore = (TextView)findViewById(R.id.tvYourScoreValue);
        tvAverageScore = (TextView)findViewById(R.id.tvAverageScoreValue);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        chkSharePerformanceData.setOnCheckedChangeListener(this);
        spnTimePeriod.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new String[]{"All Time", "Past Year", "Past 6 Months", "Past 3 Months", "Past Month", "Past 2 Weeks", "Past Week", "Past 3 Days", "Past Day"}));
        spnTimePeriod.setOnItemSelectedListener(this);
        spnCourse.setOnItemSelectedListener(this);
        CourseData.getStudentCourses(getStudentCoursesOnSuccess, getStudentCoursesOnError);
        PerformanceData.getStudentPerformances(spnTimePeriod.getSelectedItem().toString(), getStudentPerformancesOnSuccess, getStudentPerformancesOnError);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
    {
        switch(parent.getId())
        {
            case R.id.spnTimePeriod:
                if(selectedCourse != null)
                {
                    PerformanceData.getCoursePerformances(selectedCourse.toString(), spnTimePeriod.getSelectedItem().toString(), getCoursePerformancesOnSuccess, getCoursePerformancesOnError);
                    PerformanceData.getStudentPerformances(spnTimePeriod.getSelectedItem().toString(), getStudentPerformancesOnSuccess, getStudentPerformancesOnError);
                }
                break;
            case R.id.spnCourse:
                if(spnTimePeriod.getSelectedItem() != null)
                {
                    selectedCourse = (Course)parent.getSelectedItem();
                    silentSetSharePerformanceDataChecked(false);
                    PerformanceData.getPerformanceDataSharedValue(selectedCourse.toString(), getPerformanceDataSharedOnSuccess, getPerformanceDataSharedOnError);
                    PerformanceData.getCoursePerformances(selectedCourse.toString(), spnTimePeriod.getSelectedItem().toString(), getCoursePerformancesOnSuccess, getCoursePerformancesOnError);
                }
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent)
    {
        if(parent.getId() == R.id.spnCourse)
        {
            selectedCourse = null;
            silentSetSharePerformanceDataChecked(false);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        if(spnCourse.getSelectedItem() == null)
        {
            silentSetSharePerformanceDataChecked(!buttonView.isChecked());
            Toast.makeText(this, R.string.no_course_selected, Toast.LENGTH_SHORT).show();
        }
        else
        {
            PerformanceData.setPerformanceDataSharedValue(spnCourse.getSelectedItem().toString(), isChecked, setPerformanceDataSharedValueOnAbort);
        }
    }

    private void silentSetSharePerformanceDataChecked(boolean checked)
    {
        chkSharePerformanceData.setOnCheckedChangeListener(null);
        chkSharePerformanceData.setChecked(checked);
        chkSharePerformanceData.setOnCheckedChangeListener(this);
    }

    private static String writeBestCourses(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> bestCourses = Performance.getBestPerformances(performances);
        return Performance.performancesToString(bestCourses);
    }

    private static String writeWorstCourses(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> worstCourses = Performance.getWorstPerformances(performances);
        return Performance.performancesToString(worstCourses);
    }

    @SuppressWarnings("unchecked")
    private ListRunnable<Course> getStudentCoursesOnSuccess = new ListRunnable<Course>()
    {
        @Override
        public void run(List<Course> courses)
        {
            spnCourse.setAdapter(new ArrayAdapter<>(YourPerformanceActivity.this, android.R.layout.simple_spinner_item, courses));
            if(selectedCourse != null)
            {
                ArrayAdapter<Course> adapter = (ArrayAdapter<Course>)spnCourse.getAdapter();
                spnCourse.setSelection(adapter.getPosition(selectedCourse));
            }
        }
    };

    private SingleArgRunnable<FirebaseError> getStudentCoursesOnError = new SingleArgRunnable<FirebaseError>()
    {
        @Override
        public void run(FirebaseError firebaseError)
        {
            tvErrorMessage.setText(firebaseError.getMessage());
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private HashMapRunnable<String, Performance> getCoursePerformancesOnSuccess = new HashMapRunnable<String, Performance>()
    {
        @Override
        public void run(HashMap<String, Performance> performances)
        {
            if(performances == null || performances.isEmpty())
            {
                tvYourScore.setText(R.string.not_yet_available);
                tvAverageScore.setText(R.string.not_yet_available);
            }
            else
            {
                Performance performance = performances.get(LoginData.getUsername());
                tvYourScore.setText(performance == null ? getString(R.string.not_yet_available) : performance.writeScore());
                tvAverageScore.setText(Performance.writeAverageScore(performances));
            }
        }
    };

    private SingleArgRunnable<String> getCoursePerformancesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private HashMapRunnable<String, Performance> getStudentPerformancesOnSuccess = new HashMapRunnable<String, Performance>()
    {
        @Override
        public void run(HashMap<String, Performance> performances)
        {
            tvBestCourses.setText(writeBestCourses(performances));
            tvWorstCourses.setText(writeWorstCourses(performances));
        }
    };

    private SingleArgRunnable<String> getStudentPerformancesOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private SingleArgRunnable<Boolean> getPerformanceDataSharedOnSuccess = new SingleArgRunnable<Boolean>()
    {
        @Override
        public void run(Boolean shared)
        {
            silentSetSharePerformanceDataChecked(shared);
        }
    };

    private SingleArgRunnable<String> getPerformanceDataSharedOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvErrorMessage.setText(text);
            tvErrorMessage.setVisibility(View.VISIBLE);
        }
    };

    private Runnable setPerformanceDataSharedValueOnAbort = new Runnable()
    {
        @Override
        public void run()
        {
            silentSetSharePerformanceDataChecked(!chkSharePerformanceData.isChecked());
            Toast.makeText(YourPerformanceActivity.this, R.string.no_option_as_no_perf_data, Toast.LENGTH_SHORT).show();
        }
    };

}
