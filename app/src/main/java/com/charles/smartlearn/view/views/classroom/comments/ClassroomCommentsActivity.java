package com.charles.smartlearn.view.views.classroom.comments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.view.templates.AppActivity;
import com.charles.smartlearn.view.views.classroom.ClassroomActivity;

public class ClassroomCommentsActivity extends AppActivity implements OnClickListener
{

    private static ClassroomCommentsActivity instance;

    protected ClassroomCommentsListView classroomCommentsListView;
    protected ClassroomCommentFormView classroomCommentFormView;
    protected ViewFlipper vfComments;

    public static ClassroomCommentsActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        setContentView(R.layout.activity_classroom_comments);
        classroomCommentsListView = new ClassroomCommentsListView(this);
        classroomCommentFormView = new ClassroomCommentFormView(this);
        vfComments = (ViewFlipper)findViewById(R.id.vfComments);
    }

    @Override
    public void onClick(View v)
    {
        ClassroomActivity.getInstance().onClick(v);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        classroomCommentFormView.onActivityResult(requestCode, resultCode, data);
    }

    public void onUpButtonPressed()
    {
        switch(vfComments.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfComments.setDisplayedChild(0);
                break;
        }
    }

}
