package com.charles.smartlearn.view.views.main;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.LoginData;
import com.charles.smartlearn.processing.database.FirebaseData;
import com.charles.smartlearn.view.templates.AppActivity;
import com.firebase.client.AuthData;

public class LoginActivity extends AppActivity implements OnClickListener
{

    private Button btnLogin;
    private Button btnResetPassword;
    private EditText etEmail;
    private EditText etPassword;
    private ProgressBar pbProgress;
    private TextView tvSuccessMessage;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnResetPassword = (Button)findViewById(R.id.btnResetPassword);
        etEmail = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        pbProgress = (ProgressBar)findViewById(R.id.pbProgress);
        tvSuccessMessage = (TextView)findViewById(R.id.tvSuccessMessage);
        tvErrorMessage = (TextView)findViewById(R.id.tvErrorMessage);
        btnLogin.setOnClickListener(this);
        btnResetPassword.setOnClickListener(this);
        setTitle(R.string.login_title);
        checkLoginState();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        etEmail.setText("");
        etPassword.setText("");
        etEmail.requestFocus();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnLogin:
                login();
                break;
            case R.id.btnResetPassword:
                resetPassword();
                break;
        }
    }

    @Override
    protected void checkLoginState()
    {
        setActionInProgress();
        AuthData authData = FirebaseData.baseRef.getAuth();
        if(authData == null)
        {
            setActionComplete();
        }
        else
        {
            LoginData.setEmail(authData.getProviderData().get("email").toString());
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
        }
    }

    private void login()
    {
        etEmail.setError(null);
        etPassword.setError(null);
        if(!validLoginInputs())
        {
            return;
        }
        setActionInProgress();
        LoginData.login(etEmail.getText().toString(), etPassword.getText().toString(), loginOnSuccess, loginOnError);
    }

    private void resetPassword()
    {
        etEmail.setError(null);
        etPassword.setError(null);
        if(!validResetPasswordInput())
        {
            return;
        }
        setActionInProgress();
        LoginData.resetPassword(etEmail.getText().toString(), resetPasswordOnSuccess, resetPasswordOnError);
    }

    private boolean validLoginInputs()
    {
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        if(etPassword.getText().toString().isEmpty())
        {
            etPassword.requestFocus();
            etPassword.setError(getString(R.string.password_empty));
            return false;
        }
        return true;
    }

    private boolean validResetPasswordInput()
    {
        if(etEmail.getText().toString().isEmpty())
        {
            etEmail.requestFocus();
            etEmail.setError(getString(R.string.email_empty));
            return false;
        }
        return true;
    }

    private void setActionInProgress()
    {
        hideKeyboard();
        tvErrorMessage.setText("");
        tvSuccessMessage.setText("");
        pbProgress.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnResetPassword.setEnabled(false);
    }

    private void setActionComplete()
    {
        pbProgress.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnResetPassword.setEnabled(true);
    }

    private SingleArgRunnable<String> checkLoginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setUsername(text);
            setActionComplete();
            goToLobby();
        }
    };

    private SingleArgRunnable<String> checkLoginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            FirebaseData.baseRef.unauth();
            tvErrorMessage.setText(text);
            setActionComplete();
        }
    };

    private SingleArgRunnable<String> loginOnSuccess = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            LoginData.setEmail(text);
            LoginData.getUsernameFromEmail(LoginData.getEmail(), checkLoginOnSuccess, checkLoginOnError);
        }
    };

    private SingleArgRunnable<String> loginOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            tvErrorMessage.setText(message);
            setActionComplete();
        }
    };

    private Runnable resetPasswordOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            tvSuccessMessage.setText(R.string.new_password_sent);
            setActionComplete();
        }
    };

    private SingleArgRunnable<String> resetPasswordOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String message)
        {
            tvErrorMessage.setText(message);
            setActionComplete();
        }
    };

}