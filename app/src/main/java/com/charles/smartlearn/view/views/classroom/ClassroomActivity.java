package com.charles.smartlearn.view.views.classroom;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.view.templates.TabbedActivity;
import com.charles.smartlearn.view.views.classroom.comments.ClassroomCommentsActivity;
import com.charles.smartlearn.view.views.classroom.questions.ClassroomQuestionsActivity;

public class ClassroomActivity extends TabbedActivity
{

    private static ClassroomActivity instance;

    public static ClassroomActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_classroom);
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        tvActionBarTitle.setText(R.string.classroom_title);
        tvActionBarCourse.setText(LearnApplication.getInstance().getSelectedCourse().getCode());
        tvActionBarCourse.setVisibility(View.VISIBLE);
        createTab(getString(R.string.questions_title), ClassroomQuestionsActivity.class);
        createTab(getString(R.string.comments_title), ClassroomCommentsActivity.class);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("currentTab", thTabbedView.getCurrentTab());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        thTabbedView.setCurrentTab(savedInstanceState.getInt("currentTab"));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        ClassroomCommentsActivity.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onUpButtonPressed()
    {
        switch(thTabbedView.getCurrentTab())
        {
            case 0:
                ClassroomQuestionsActivity.getInstance().onUpButtonPressed();
                break;
            case 1:
                ClassroomCommentsActivity.getInstance().onUpButtonPressed();
                break;
        }
    }

}
