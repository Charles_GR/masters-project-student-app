package com.charles.smartlearn.view.views.revision.quizzes;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.revision.quizzes.Quiz;
import com.charles.smartlearn.view.extras.DataCharts;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import java.util.HashMap;
import java.util.List;

public class RevisionQuizResultsView implements OnClickListener, OnCheckedChangeListener
{

    private RevisionQuizzesActivity parent;
    private BarChart bcResults;
    private Button btnViewQuestionResults;
    private Button btnViewChart;
    private Button btnPreviousQuestionB;
    private Button btnNextQuestionB;
    private PieChart pcResults;
    private Switch swChartType;
    private TextView tvQuizTitle;
    private TextView tvBestScore;
    private TextView tvWorstScore;
    private TextView tvAverageScore;
    private TextView tvQuestionB;
    private TextView tvResults;
    private ViewFlipper vfDataCharts;

    public RevisionQuizResultsView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        bcResults = (BarChart)parent.findViewById(R.id.bcResults);
        btnViewQuestionResults = (Button)parent.findViewById(R.id.btnViewQuestionResults);
        btnViewChart = (Button)parent.findViewById(R.id.btnViewChart);
        btnPreviousQuestionB = (Button)parent.findViewById(R.id.btnPreviousQuestionB);
        btnNextQuestionB = (Button)parent.findViewById(R.id.btnNextQuestionB);
        pcResults = (PieChart)parent.findViewById(R.id.pcResults);
        swChartType = (Switch)parent.findViewById(R.id.swChartType);
        tvQuizTitle = (TextView)parent.findViewById(R.id.tvQuizTitle);
        tvBestScore = (TextView)parent.findViewById(R.id.tvBestScoreValue);
        tvWorstScore = (TextView)parent.findViewById(R.id.tvWorstScoreValue);
        tvAverageScore = (TextView)parent.findViewById(R.id.tvAverageScoreValue);
        tvQuestionB = (TextView)parent.findViewById(R.id.tvQuestionB);
        tvResults = (TextView)parent.findViewById(R.id.tvResults);
        vfDataCharts = (ViewFlipper)parent.findViewById(R.id.vfDataCharts);
        btnViewQuestionResults.setOnClickListener(this);
        btnViewChart.setOnClickListener(this);
        btnPreviousQuestionB.setOnClickListener(this);
        btnNextQuestionB.setOnClickListener(this);
        swChartType.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnViewQuestionResults:
                parent.questionIndex = 0;
                viewQuestionResults();
                parent.vfQuizzes.setDisplayedChild(4);
                break;
            case R.id.btnViewChart:
                viewChart();
                break;
            case R.id.btnPreviousQuestionB:
                parent.questionIndex--;
                viewQuestionResults();
                break;
            case R.id.btnNextQuestionB:
                parent.questionIndex++;
                viewQuestionResults();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
    {
        vfDataCharts.setDisplayedChild(swChartType.isChecked() ? 1 : 0);
    }

    private void viewQuestionResults()
    {
        parent.question = parent.quiz.getQuestions().get(parent.questionIndex);
        displayResults();
        btnPreviousQuestionB.setEnabled(parent.questionIndex > 0);
        btnNextQuestionB.setEnabled(parent.questionIndex < parent.quiz.getQuestions().size() - 1);
    }

    @SuppressWarnings("unchecked")
    private void viewChart()
    {
        HashMap<String, Integer> answerCounts = parent.question.calcAnswerCounts(parent.quiz.getResponses(), parent.questionIndex);
        if(answerCounts.isEmpty())
        {
            Toast.makeText(parent, R.string.no_chart_data_available, Toast.LENGTH_SHORT).show();
        }
        else
        {
            parent.vfQuizzes.setDisplayedChild(5);
            DataCharts.drawBarChart(bcResults, answerCounts);
            DataCharts.drawPieChart(pcResults, answerCounts);
        }
    }

    protected void displayResults(List<Quiz> quizzes)
    {
        if(parent.quiz == null)
        {
            return;
        }
        for(Quiz quiz : quizzes)
        {
            if(quiz.getKey().equals(parent.quiz.getKey()))
            {
                parent.quiz = quiz;
                displayResults();
                break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected void displayResults()
    {
        tvQuizTitle.setText(parent.quiz.getTitle());
        tvBestScore.setText(parent.quiz.getResponses() == null ? parent.getString(R.string.not_yet_available) : parent.quiz.writeBestScore());
        tvWorstScore.setText(parent.quiz.getResponses() == null ? parent.getString(R.string.not_yet_available) : parent.quiz.writeWorstScore());
        tvAverageScore.setText(parent.quiz.getResponses() == null ? parent.getString(R.string.not_yet_available) : parent.quiz.writeAverageScore());
        if(parent.question != null)
        {
            tvQuestionB.setText(parent.question.getQuestion());
            tvResults.setText(parent.question.writeResults(parent.quiz.getResponses(), parent.questionIndex));
            HashMap<String, Integer> answerCounts = parent.question.calcAnswerCounts(parent.quiz.getResponses(), parent.questionIndex);
            DataCharts.drawBarChart(bcResults, answerCounts);
            DataCharts.drawPieChart(pcResults, answerCounts);
        }
    }

}