package com.charles.smartlearn.view.extras;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;

public abstract class PopupDialogs
{

    public static void showInputDialog(Context context, String title, String inputLabel, String positiveButtonText, String negativeButtonText, final SingleArgRunnable<String> onPositiveButtonPressed)
    {
        Builder alertDialogBuilder = new Builder(context);
        alertDialogBuilder.setTitle(title);
        View inputView = LayoutInflater.from(context).inflate(R.layout.layout_input_popup, null);
        alertDialogBuilder.setView(inputView);
        TextView tvInputLabel = (TextView)inputView.findViewById(R.id.tvInputLabel);
        final EditText etInputValue = (EditText)inputView.findViewById(R.id.etInputValue);
        tvInputLabel.setText(inputLabel);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(positiveButtonText, new OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                onPositiveButtonPressed.run(etInputValue.getText().toString());
            }
        });
        alertDialogBuilder.setNegativeButton(negativeButtonText, new OnClickListener()
        {
            public void onClick(DialogInterface dialog,int id)
            {
                dialog.cancel();
            }
        });
        alertDialogBuilder.create().show();
    }

}
