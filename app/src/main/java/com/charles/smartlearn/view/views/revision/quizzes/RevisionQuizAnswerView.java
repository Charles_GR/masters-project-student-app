package com.charles.smartlearn.view.views.revision.quizzes;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.quiz.QuizMaster;

public class RevisionQuizAnswerView implements OnClickListener
{

    private RevisionQuizzesActivity parent;
    private Button btnCheckAnswer;
    private LinearLayout llSelectAnswer;
    private TextView tvQuestionA;
    private TextView tvQuizSummary;

    public RevisionQuizAnswerView(RevisionQuizzesActivity parent)
    {
        this.parent = parent;
        btnCheckAnswer = (Button)parent.findViewById(R.id.btnCheckAnswer);
        llSelectAnswer = (LinearLayout)parent.findViewById(R.id.llSelectAnswer);
        tvQuestionA = (TextView)parent.findViewById(R.id.tvQuestionA);
        tvQuizSummary = (TextView)parent.findViewById(R.id.tvQuizSummary);
        btnCheckAnswer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if(v.getId() == R.id.btnCheckAnswer)
        {
            checkAnswer();
        }
    }

    protected void displayQuestion()
    {
        tvQuestionA.setText(QuizMaster.getInstance().getCurrentQuestion().getQuestion());
        llSelectAnswer.removeAllViews();
        for(Object obj : QuizMaster.getInstance().getCurrentQuestion().createAnswerViews(parent))
        {
            llSelectAnswer.addView((View)obj);
        }
        tvQuizSummary.setText(parent.getString(R.string.quiz_summary, QuizMaster.getInstance().getQuestionsAnswered(), QuizMaster.getInstance().getNumberOfQuestions()));
    }

    private void checkAnswer()
    {
        QuizMaster.getInstance().addQuestionAnswered();
        QuizMaster.getInstance().addToPointsPossible();
        QuizMaster.getInstance().addToPointsAwarded(llSelectAnswer);
        QuizMaster.getInstance().addQuestionResult(llSelectAnswer);
        QuizMaster.getInstance().addQuestionResponse(llSelectAnswer);
        advanceQuiz();
    }

    private void advanceQuiz()
    {
        if(QuizMaster.getInstance().hasNextQuestion())
        {
            QuizMaster.getInstance().goToNextQuestion();
            displayQuestion();
        }
        else
        {
            QuizMaster.getInstance().saveQuizResponse();
            QuizMaster.getInstance().updatePerformance();
            parent.vfQuizzes.setDisplayedChild(2);
            parent.revisionQuizFeedbackView.displayResults();
        }
    }

}
