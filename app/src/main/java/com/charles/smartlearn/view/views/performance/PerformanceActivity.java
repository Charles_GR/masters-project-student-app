package com.charles.smartlearn.view.views.performance;

import android.os.Bundle;
import android.view.View;
import com.charles.smartlearn.R;
import com.charles.smartlearn.view.templates.TabbedActivity;

public class PerformanceActivity extends TabbedActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.activity_performance);
        super.onCreate(savedInstanceState);
        tvActionBarTitle.setText(R.string.performance_title);
        tvActionBarCourse.setVisibility(View.GONE);
        createTab(getString(R.string.yours), YourPerformanceActivity.class);
        createTab(getString(R.string.class_), ClassPerformanceActivity.class);
    }

    @Override
    protected void onUpButtonPressed()
    {
        goToLobby();
    }

}
