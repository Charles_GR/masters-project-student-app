package com.charles.smartlearn.view.views.revision.notes;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.database.revision.notes.Note;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.RevisionData;
import com.charles.smartlearn.processing.media.CheckVideoExistsTask;
import com.charles.smartlearn.view.extras.PopupDialogs;
import com.charles.smartlearn.view.views.revision.RevisionActivity;
import java.io.File;

public class RevisionNoteFormView implements OnClickListener
{

    private static final int IMAGE_SIZE_LIMIT = 314572800;

    private RevisionNotesActivity parent;
    private Button btnAddImage;
    private Button btnAddVideo;
    private Button btnPreviewNote;
    private Button btnPostNote;
    private Button btnEditNotePreview;
    private Button btnPostNotePreview;
    private EditText etTitle;
    private EditText etContent;
    private LinearLayout llNotePreviewContent;
    private ProgressBar pbPostNotePreview;
    private ProgressBar pbPostNote;
    private TextView tvNotePreviewTitle;
    private TextView tvPostNotePreviewErrorMessage;
    private TextView tvPostNoteErrorMessage;

    public RevisionNoteFormView(RevisionNotesActivity parent)
    {
        this.parent = parent;
        btnAddImage = (Button)parent.findViewById(R.id.btnAddImage);
        btnAddVideo = (Button)parent.findViewById(R.id.btnAddVideo);
        btnPreviewNote = (Button)parent.findViewById(R.id.btnPreviewNote);
        btnPostNote = (Button)parent.findViewById(R.id.btnPostNote);
        btnEditNotePreview = (Button)parent.findViewById(R.id.btnEditNotePreview);
        btnPostNotePreview = (Button)parent.findViewById(R.id.btnPostNotePreview);
        etTitle = (EditText)parent.findViewById(R.id.etTitle);
        etContent = (EditText)parent.findViewById(R.id.etContent);
        llNotePreviewContent = (LinearLayout)parent.findViewById(R.id.llNotePreviewContent);
        pbPostNote = (ProgressBar)parent.findViewById(R.id.pbPostNote);
        pbPostNotePreview = (ProgressBar)parent.findViewById(R.id.pbPostNotePreview);
        tvNotePreviewTitle = (TextView)parent.findViewById(R.id.tvNotePreviewTitle);
        tvPostNotePreviewErrorMessage = (TextView)parent.findViewById(R.id.tvPostNotePreviewErrorMessage);
        tvPostNoteErrorMessage = (TextView)parent.findViewById(R.id.tvPostNoteErrorMessage);
        btnAddImage.setOnClickListener(this);
        btnAddVideo.setOnClickListener(this);
        btnPreviewNote.setOnClickListener(this);
        btnPostNote.setOnClickListener(this);
        btnEditNotePreview.setOnClickListener(this);
        btnPostNotePreview.setOnClickListener(this);
    }

    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putString("currentNoteTitle", tvNotePreviewTitle.getText().toString().split(" by")[0]);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode != Activity.RESULT_OK || data == null)
        {
            return;
        }
        try
        {
            Cursor cursor = parent.getContentResolver().query(data.getData(), new String[]{MediaStore.Images.Media.DATA}, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
            if(new File(imagePath).length() > IMAGE_SIZE_LIMIT)
            {
                Toast.makeText(parent, R.string.selected_image_too_large, Toast.LENGTH_SHORT).show();
                return;
            }
            etContent.append("#<image>" + RevisionData.addImage(imagePath) + "</image>#");
        }
        catch(Exception e)
        {
            Toast.makeText(parent, R.string.failed_to_load_image, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddImage:
                RevisionActivity.getInstance().startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
                break;
            case R.id.btnAddVideo:
                addVideo();
                break;
            case R.id.btnPreviewNote:
                previewNote();
                break;
            case R.id.btnPostNote:
                postNote();
                break;
            case R.id.btnEditNotePreview:
                parent.vfNotes.setDisplayedChild(3);
                break;
            case R.id.btnPostNotePreview:
                postNotePreview();
                break;
        }
    }

    protected void editNote(Note note)
    {
        etTitle.setError(null);
        etContent.setError(null);
        parent.currentNoteKey = note.getKey();
        etTitle.setText(note.getTitle());
        etContent.setText(note.getContent());
        parent.vfNotes.setDisplayedChild(3);
    }

    private void previewNote()
    {
        etTitle.setError(null);
        etContent.setError(null);
        if(!sendNoteValidInputs())
        {
            return;
        }
        parent.currentNoteContent = etContent.getText().toString();
        Note note = new Note(null, etTitle.getText().toString(), etContent.getText().toString());
        note.createNoteViews(parent, LearnApplication.getInstance().getImages(), tvNotePreviewTitle, llNotePreviewContent);
        parent.vfNotes.setDisplayedChild(4);
    }


    private void addVideo()
    {
        PopupDialogs.showInputDialog(parent, "Add Video", "YouTube Video ID:", "Add", "Cancel", onVideoInputEntered);
    }

    public void appendVideo(String videoID)
    {
        etContent.append("#<video>" + videoID + "</video>#");
    }

    private void postNote()
    {
        etTitle.setError(null);
        etContent.setError(null);
        if(!sendNoteValidInputs())
        {
            return;
        }
        setPostNoteInProgress();
        RevisionData.postNote(parent.currentNoteKey, etTitle.getText().toString(), etContent.getText().toString(), postNoteOnSuccess, postNoteOnError);
    }

    private void postNotePreview()
    {
        etTitle.setError(null);
        etContent.setError(null);
        if(!sendNoteValidInputs())
        {
            return;
        }
        setPostNotePreviewInProgress();
        RevisionData.postNote(parent.currentNoteKey, etTitle.getText().toString(), etContent.getText().toString(), postNotePreviewOnSuccess, postNotePreviewOnError);
    }

    protected void createNoteViews(Note note)
    {
        note.createNoteViews(parent, LearnApplication.getInstance().getImages(), tvNotePreviewTitle, llNotePreviewContent);
    }

    protected void clearNoteForm()
    {
        etTitle.setError(null);
        etContent.setError(null);
        etTitle.setText("");
        etContent.setText("");
    }

    private void setPostNoteInProgress()
    {
        tvPostNoteErrorMessage.setText("");
        tvPostNoteErrorMessage.setVisibility(View.GONE);
        pbPostNote.setVisibility(View.VISIBLE);
        etTitle.setError(null);
        etContent.setError(null);
        etTitle.setEnabled(false);
        etContent.setEnabled(false);
        btnAddImage.setEnabled(false);
        btnAddVideo.setEnabled(false);
        btnPreviewNote.setEnabled(false);
        btnPostNote.setEnabled(false);
    }

    private void setPostNoteComplete()
    {
        etTitle.setEnabled(true);
        etContent.setEnabled(true);
        btnAddImage.setEnabled(true);
        btnAddVideo.setEnabled(true);
        btnPreviewNote.setEnabled(true);
        btnPostNote.setEnabled(true);
        pbPostNote.setVisibility(View.GONE);
    }

    private void setPostNotePreviewInProgress()
    {
        tvPostNotePreviewErrorMessage.setText("");
        tvPostNotePreviewErrorMessage.setVisibility(View.GONE);
        pbPostNotePreview.setVisibility(View.VISIBLE);
        etTitle.setError(null);
        etContent.setError(null);
        btnEditNotePreview.setEnabled(false);
        btnPostNotePreview.setEnabled(false);
    }

    private void setPostNotePreviewComplete()
    {
        etTitle.setEnabled(true);
        etContent.setEnabled(true);
        btnEditNotePreview.setEnabled(true);
        btnPostNotePreview.setEnabled(true);
        pbPostNotePreview.setVisibility(View.GONE);
    }

    private boolean sendNoteValidInputs()
    {
        if(etTitle.getText().toString().isEmpty())
        {
            etTitle.requestFocus();
            etTitle.setError(parent.getString(R.string.title_empty));
            return false;
        }
        if(etContent.getText().toString().isEmpty())
        {
            etContent.requestFocus();
            etContent.setError(parent.getString(R.string.content_empty));
            return false;
        }
        return true;
    }

    private SingleArgRunnable<String> onVideoInputEntered = new SingleArgRunnable<String>()
    {
        @Override
        public void run(final String text)
        {
            if(text.isEmpty())
            {
                Toast.makeText(parent, R.string.no_video_ID_entered, Toast.LENGTH_SHORT).show();
            }
            else
            {
                new CheckVideoExistsTask(parent, text).execute();
            }
        }
    };

    private Runnable postNotePreviewOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            parent.vfNotes.setDisplayedChild(0);
            setPostNotePreviewComplete();
            LearnApplication.getInstance().getImages().clear();
        }
    };

    private SingleArgRunnable<String> postNotePreviewOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvPostNotePreviewErrorMessage.setText(text);
            tvPostNotePreviewErrorMessage.setVisibility(View.VISIBLE);
            setPostNotePreviewComplete();
        }
    };

    private Runnable postNoteOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            parent.vfNotes.setDisplayedChild(0);
            setPostNoteComplete();
            LearnApplication.getInstance().getImages().clear();
        }
    };

    private SingleArgRunnable<String> postNoteOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            tvPostNoteErrorMessage.setText(text);
            tvPostNoteErrorMessage.setVisibility(View.VISIBLE);
            setPostNoteComplete();
        }
    };

}
