package com.charles.smartlearn.view.views.revision.quizzes;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.revision.quizzes.*;
import com.charles.smartlearn.view.templates.AppActivity;
import com.charles.smartlearn.view.views.revision.RevisionActivity;

public class RevisionQuizzesActivity extends AppActivity implements OnClickListener
{

    private static RevisionQuizzesActivity instance;

    protected RevisionQuizzesListView revisionQuizzesListView;
    protected RevisionQuizAnswerView revisionQuizAnswerView;
    protected RevisionQuizFeedbackView revisionQuizFeedbackView;
    protected RevisionQuizResultsView revisionQuizResultsView;
    protected ViewFlipper vfQuizzes;

    protected Quiz quiz;
    protected Question question;
    protected int questionIndex;

    public static RevisionQuizzesActivity getInstance()
    {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(!checkCourseIsSelected())
        {
            return;
        }
        instance = this;
        setContentView(R.layout.activity_revision_quizzes);
        revisionQuizzesListView = new RevisionQuizzesListView(this);
        revisionQuizAnswerView = new RevisionQuizAnswerView(this);
        revisionQuizFeedbackView = new RevisionQuizFeedbackView(this);
        revisionQuizResultsView = new RevisionQuizResultsView(this);
        vfQuizzes = (ViewFlipper)findViewById(R.id.vfQuizzes);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("currentQuizView", vfQuizzes.getDisplayedChild());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        vfQuizzes.setDisplayedChild(savedInstanceState.getInt("currentQuizView"));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v)
    {
        RevisionActivity.getInstance().onClick(v);
    }

    public void onUpButtonPressed()
    {
        switch(vfQuizzes.getDisplayedChild())
        {
            case 0:
                goToLobby();
                break;
            case 1:
                vfQuizzes.setDisplayedChild(0);
                break;
            case 2:
                vfQuizzes.setDisplayedChild(0);
                break;
            case 3:
                vfQuizzes.setDisplayedChild(0);
                break;
            case 4:
                vfQuizzes.setDisplayedChild(3);
                break;
            case 5:
                vfQuizzes.setDisplayedChild(4);
                break;
        }
    }

}
