package com.charles.smartlearn.view.views.classroom.comments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.ClassroomData;
import com.charles.smartlearn.view.views.classroom.ClassroomActivity;
import java.io.File;

public class ClassroomCommentFormView implements OnClickListener
{

    private static final int IMAGE_SIZE_LIMIT = 314572800;

    private ClassroomCommentsActivity parent;
    private Button btnAddImage;
    private Button btnRemoveImage;
    private Button btnPostComment;
    private EditText etComment;
    private ImageView ivImage;
    private ProgressBar pbPostComment;
    private TextView tvPostCommentErrorMessage;

    private String imageKey;

    protected ClassroomCommentFormView(ClassroomCommentsActivity parent)
    {
        this.parent = parent;
        btnAddImage = (Button)parent.findViewById(R.id.btnAddImage);
        btnRemoveImage = (Button)parent.findViewById(R.id.btnRemoveImage);
        btnPostComment = (Button)parent.findViewById(R.id.btnPostComment);
        etComment = (EditText)parent.findViewById(R.id.etComment);
        ivImage = (ImageView)parent.findViewById(R.id.ivImage);
        pbPostComment = (ProgressBar)parent.findViewById(R.id.pbPostComment);
        tvPostCommentErrorMessage = (TextView)parent.findViewById(R.id.tvPostCommentErrorMessage);
        btnAddImage.setOnClickListener(this);
        btnRemoveImage.setOnClickListener(this);
        btnPostComment.setOnClickListener(this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode != Activity.RESULT_OK || data == null)
        {
            return;
        }
        try
        {
            Cursor cursor = parent.getContentResolver().query(data.getData(), new String[]{MediaStore.Images.Media.DATA}, null, null, null);
            cursor.moveToFirst();
            String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
            cursor.close();
            if(new File(imagePath).length() > IMAGE_SIZE_LIMIT)
            {
                Toast.makeText(parent, R.string.selected_image_too_large, Toast.LENGTH_SHORT).show();
                return;
            }
            imageKey = ClassroomData.addImage(imagePath);
            ivImage.setImageBitmap(BitmapFactory.decodeFile(imagePath));
        }
        catch(Exception e)
        {
            Toast.makeText(parent, R.string.failed_to_load_image, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.btnAddImage:
                ClassroomActivity.getInstance().startActivityForResult(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), 0);
                break;
            case R.id.btnRemoveImage:
                imageKey = null;
                ivImage.setImageDrawable(null);
                break;
            case R.id.btnPostComment:
                postComment();
                break;
        }
    }

    private void postComment()
    {
        etComment.setError(null);
        parent.hideKeyboard();
        if(etComment.getText().toString().isEmpty())
        {
            etComment.requestFocus();
            etComment.setError(parent.getString(R.string.comment_empty));
            return;
        }
        setPostCommentInProgress();
        ClassroomData.postComment(etComment.getText().toString(), imageKey, postCommentOnSuccess, postCommentOnError);
    }

    protected void clearCommentForm()
    {
        etComment.setError(null);
        etComment.setText("");
        imageKey = null;
        ivImage.setImageDrawable(null);
    }

    private void setPostCommentInProgress()
    {
        pbPostComment.setVisibility(View.VISIBLE);
        btnAddImage.setEnabled(false);
        btnRemoveImage.setEnabled(false);
        btnPostComment.setEnabled(false);
    }

    private void setPostCommentComplete()
    {
        pbPostComment.setVisibility(View.GONE);
        btnAddImage.setEnabled(true);
        btnRemoveImage.setEnabled(true);
        btnPostComment.setEnabled(true);
    }

    private Runnable postCommentOnSuccess = new Runnable()
    {
        @Override
        public void run()
        {
            setPostCommentComplete();
            etComment.setText("");
            ivImage.setImageDrawable(null);
            imageKey = null;
            tvPostCommentErrorMessage.setText("");
            tvPostCommentErrorMessage.setVisibility(View.GONE);
            parent.vfComments.setDisplayedChild(0);
        }
    };

    private SingleArgRunnable<String> postCommentOnError = new SingleArgRunnable<String>()
    {
        @Override
        public void run(String text)
        {
            setPostCommentComplete();
            tvPostCommentErrorMessage.setText(text);
            tvPostCommentErrorMessage.setVisibility(View.VISIBLE);
        }
    };

}
