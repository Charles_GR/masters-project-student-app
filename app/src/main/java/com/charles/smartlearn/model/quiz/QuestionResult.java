package com.charles.smartlearn.model.quiz;

import java.util.ArrayList;
import java.util.List;

public class QuestionResult
{

    private String question;
    private List<AnswerResult> answersResults;

    public QuestionResult(String question)
    {
        this.question = question;
        answersResults = new ArrayList<>();
    }

    public boolean isEmpty()
    {
        return answersResults.isEmpty();
    }

    public void addFeedback(String answer, boolean selected, boolean correct, String feedback)
    {
        answersResults.add(new AnswerResult(answer, selected, correct, feedback));
    }

    public String writeFeedback()
    {
        String feedback = "The question was: " + question + "\n\n";
        for(AnswerResult answerResult : answersResults)
        {
            if(answerResult.getSelected() && answerResult.getCorrect())
            {
                feedback += "You selected " + answerResult.getAnswer() + " and this was a correct answer. ";
            }
            else if(answerResult.getSelected() && !answerResult.getCorrect())
            {
                feedback += "You selected " + answerResult.getAnswer() + " but this was an incorrect answer. ";
            }
            else if(!answerResult.getSelected() && answerResult.getCorrect())
            {
                feedback += "You did not select " + answerResult.getAnswer() + " but this was a correct answer. ";
            }
            else if(!answerResult.getSelected() && !answerResult.getCorrect())
            {
                feedback += "You did not select " + answerResult.getAnswer() + " and this was an incorrect answer. ";
            }
            feedback += "Here is some feedback from the teacher for this answer: ";
            feedback += answerResult.getFeedback() + "\n\n";
        }
        return feedback.trim();
    }
    
}
