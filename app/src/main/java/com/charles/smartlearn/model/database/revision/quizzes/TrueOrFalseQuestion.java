package com.charles.smartlearn.model.database.revision.quizzes;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.quiz.QuestionResult;
import java.util.*;

public class TrueOrFalseQuestion extends Question<Boolean>
{

    public static final String QUESTION_TYPE_TAG = "True Or False";

    private Boolean correctAnswer;
    private String feedbackForTrue;
    private String feedbackForFalse;

    public TrueOrFalseQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public Boolean getCorrectAnswer()
    {
        return correctAnswer;
    }

    public String getFeedbackForTrue()
    {
        return feedbackForTrue;
    }

    public String getFeedbackForFalse()
    {
        return feedbackForFalse;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                String answer = response.getAnswers().get(questionIndex).toString();
                if(answer != null)
                {
                    answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        RadioGroup rgAnswers = new RadioGroup(context);
        rgAnswers.setTag("rgAnswers");
        rgAnswers.addView(createAnswerRadioButton(context, context.getString(R.string.ans_true), 1));
        rgAnswers.addView(createAnswerRadioButton(context, context.getString(R.string.ans_false), 2));
        return Arrays.asList(new View[]{rgAnswers});
    }

    @Override
    public Boolean findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        for(int i = 0; i < rgAnswers.getChildCount(); i++)
        {
            RadioButton rbAnswer = (RadioButton)rgAnswers.getChildAt(i);
            if(rbAnswer.isChecked())
            {
                return Boolean.parseBoolean(rbAnswer.getText().toString());
            }
        }
        return null;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswers)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswers.findViewWithTag("rgAnswers");
        RadioButton rbSelectedAnswer = (RadioButton)rgAnswers.findViewById(rgAnswers.getCheckedRadioButtonId());
        return (rbSelectedAnswer != null && Boolean.parseBoolean(rbSelectedAnswer.getText().toString()) == correctAnswer) ? 1 : 0;
    }

    @Override
    public QuestionResult getQuestionResult(LinearLayout llSelectAnswer)
    {
        QuestionResult questionResult = new QuestionResult(question);
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        RadioButton rbSelectedAnswer = (RadioButton)rgAnswers.findViewById(rgAnswers.getCheckedRadioButtonId());
        if(rbSelectedAnswer != null)
        {
            boolean answer = Boolean.parseBoolean(rbSelectedAnswer.getText().toString());
            questionResult.addFeedback(rbSelectedAnswer.getText().toString(), true, answer == correctAnswer, answer ? feedbackForTrue : feedbackForFalse);
        }
        return questionResult;
    }

    private static RadioButton createAnswerRadioButton(Context context, String answer, int id)
    {
        RadioButton rbAnswer = new RadioButton(context);
        rbAnswer.setText(answer);
        rbAnswer.setId(id);
        return rbAnswer;
    }

}

