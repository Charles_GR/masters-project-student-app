package com.charles.smartlearn.model.runnable;

import java.util.List;

public interface ListRunnable<T> extends SingleArgRunnable<List<T>>
{

}