package com.charles.smartlearn.model.database.students;

import java.util.HashMap;

public class Course implements Comparable<Course>
{

    private String code;
    private String title;
    private String teacher;
    private HashMap<String, String> students;

    public Course()
    {
        students = new HashMap<>();
    }

    public String getCode()
    {
        return code;
    }

    public String getTitle()
    {
        return title;
    }

    public String getTeacher()
    {
        return teacher;
    }

    public HashMap<String, String> getStudents()
    {
        return students;
    }

    @Override
    public String toString()
    {
        return code;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Course)
        {
            Course course = (Course)obj;
            return course.getCode().equals(code);
        }
        return false;
    }

    @Override
    public int compareTo(Course otherCourse)
    {
        return code.compareTo(otherCourse.code);
    }

}
