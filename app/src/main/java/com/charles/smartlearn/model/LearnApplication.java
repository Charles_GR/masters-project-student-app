package com.charles.smartlearn.model;

import android.app.Application;
import com.charles.smartlearn.model.database.students.Course;
import com.firebase.client.Firebase;
import java.util.HashMap;
import java.util.Map;

public class LearnApplication extends Application
{

    private static LearnApplication instance;

    private Map<String, String> images;
    private Course selectedCourse;

    public static LearnApplication getInstance()
    {
        return instance;
    }

    public Map<String, String> getImages()
    {
        return images;
    }

    public Course getSelectedCourse()
    {
        return selectedCourse;
    }

    public void setSelectedCourse(Course course)
    {
        selectedCourse = course;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        images = new HashMap<>();
        Firebase.setAndroidContext(this);
    }

}
