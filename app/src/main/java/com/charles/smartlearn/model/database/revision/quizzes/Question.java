package com.charles.smartlearn.model.database.revision.quizzes;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.quiz.QuestionResult;
import com.shaded.fasterxml.jackson.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = SingleSelectQuestion.class, name = SingleSelectQuestion.QUESTION_TYPE_TAG),
               @JsonSubTypes.Type(value = MultiSelectQuestion.class, name = MultiSelectQuestion.QUESTION_TYPE_TAG),
               @JsonSubTypes.Type(value = TrueOrFalseQuestion.class, name = TrueOrFalseQuestion.QUESTION_TYPE_TAG),
               @JsonSubTypes.Type(value = ShortAnswerQuestion.class, name = ShortAnswerQuestion.QUESTION_TYPE_TAG)})
public abstract class Question<AnswersType>
{

    protected String question;
    protected String type;

    protected Question(String question, String type)
    {
        this.question = question;
        this.type = type;
    }

    protected Question(String type)
    {
        this.type = type;
    }

    protected Question()
    {
    }

    public String getQuestion()
    {
        return question;
    }

    public String getType()
    {
        return type;
    }

    public abstract List<View> createAnswerViews(Context context);

    public abstract AnswersType findStudentAnswers(Context context, LinearLayout llSelectAnswer);

    public abstract int calcPointsPossible();

    public abstract int calcPointsForAnswers(LinearLayout llSelectAnswer);

    public abstract QuestionResult getQuestionResult(LinearLayout llSelectAnswer);

    public HashMap<String, Response> getNonEmptyResponses(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Response> nonEmptyResponses = new HashMap<>();
        for(Entry<String, Response> entry : responses.entrySet())
        {
            List answers = entry.getValue().getAnswers();
            if(questionIndex < answers.size() && answers.get(questionIndex) != null)
            {
                nonEmptyResponses.put(entry.getKey(), entry.getValue());
            }
        }
        return nonEmptyResponses;
    }

    public String writeResults(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Response> nonEmptyResponses = getNonEmptyResponses(responses, questionIndex);
        if(nonEmptyResponses.isEmpty())
        {
            return LearnApplication.getInstance().getString(R.string.no_results_yet);
        }
        String results = nonEmptyResponses.size() + " students have answered the question.\n\n";
        for(Entry<String, Integer> entry : calcAnswerCounts(nonEmptyResponses, questionIndex).entrySet())
        {
            results += entry.getKey() + " was given as an answer by " + entry.getValue() + " students (" + 100 * entry.getValue() / responses.size() + "%).\n\n";
        }
        return results;
    }

    public abstract HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex);

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof Question)
        {
            Question question = (Question)obj;
            return question.getQuestion().equals(this.question);
        }
        return false;
    }

}
