package com.charles.smartlearn.model.runnable;

public interface SingleArgRunnable<T>
{

    void run(T arg);

}
