package com.charles.smartlearn.model.database.classroom.questions;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import com.shaded.fasterxml.jackson.annotation.*;
import java.util.*;
import java.util.Map.Entry;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = SingleSelectQuestion.class, name = SingleSelectQuestion.QUESTION_TYPE_TAG),
               @JsonSubTypes.Type(value = MultiSelectQuestion.class, name = MultiSelectQuestion.QUESTION_TYPE_TAG),
               @JsonSubTypes.Type(value = TrueOrFalseQuestion.class, name = TrueOrFalseQuestion.QUESTION_TYPE_TAG),
               @JsonSubTypes.Type(value = ShortAnswerQuestion.class, name = ShortAnswerQuestion.QUESTION_TYPE_TAG)})
public abstract class Question<AnswersType>
{

    protected String key;
    protected String question;
    protected String type;
    protected Date dateCreated;
    protected HashMap<String, Response<AnswersType>> responses;

    protected Question(String question, String type)
    {
        this.question = question;
        this.type = type;
        dateCreated = new Date();
        responses = new HashMap<>();
    }

    protected Question(String type)
    {
        this.type = type;
        responses = new HashMap<>();
    }

    protected Question()
    {
        responses = new HashMap<>();
    }

    public String getKey()
    {
        return key;
    }

    public String getQuestion()
    {
        return question;
    }

    public String getType()
    {
        return type;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public HashMap<String, Response<AnswersType>> getResponses()
    {
        return responses;
    }

    public void setQuestion(String question)
    {
        this.question = question;
    }

    public void addResponse(Response<AnswersType> response)
    {
        responses.put(response.getStudent(), response);
    }

    public String writeResults()
    {
        String results = writeOwnAnswers() + "\n\n";
        results += responses.size() + " students have answered the question.\n\n";
        for(Entry<String, Integer> entry : calcAnswerCounts().entrySet())
        {
            results += entry.getKey() + " was given as an answer by " + entry.getValue() + " students (" + 100 * entry.getValue() / responses.size() + "%).\n\n";
        }
        return results;
    }

    public abstract HashMap<String, Integer> calcAnswerCounts();

    public abstract List<View> createAnswerViews(Context context);

    public abstract AnswersType findStudentAnswers(Context context, LinearLayout llSelectAnswer);

    public abstract int calcPointsPossible();

    public abstract int calcPointsForAnswers(LinearLayout llSelectAnswer);

    protected abstract String writeOwnAnswers();

}
