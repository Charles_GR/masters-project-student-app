package com.charles.smartlearn.model.runnable;

import java.util.HashMap;

public interface HashMapRunnable<K, V> extends SingleArgRunnable<HashMap<K, V>>
{

}
