package com.charles.smartlearn.model.database.revision.quizzes;

import java.util.ArrayList;
import java.util.List;

public abstract class SelectQuestion<AnswersType> extends Question<AnswersType>
{

    protected List<Answer> answers;

    public SelectQuestion(String question, String type)
    {
        super(question, type);
        answers = new ArrayList<>();
    }

    public SelectQuestion(String type)
    {
        super(type);
        answers = new ArrayList<>();
    }

    public List<Answer> getAnswers()
    {
        return answers;
    }

}