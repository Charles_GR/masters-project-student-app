package com.charles.smartlearn.model.database.classroom.questions;

import java.util.*;

public abstract class SelectQuestion<AnswersType> extends Question<AnswersType>
{

    protected List<Answer> answers;

    public SelectQuestion(String type)
    {
        super(type);
        answers = new ArrayList<>();
    }

    public List<Answer> getAnswers()
    {
        return answers;
    }

    public boolean addAnswer(String answer, boolean correct)
    {
        if(!containsAnswer(answer))
        {
            answers.add(new Answer(answer, correct));
            return true;
        }
        return false;
    }

    protected boolean containsAnswer(String answerText)
    {
        for(Answer answer : answers)
        {
            if(answer.getAnswer().equals(answerText))
            {
                return true;
            }
        }
        return false;
    }

}
