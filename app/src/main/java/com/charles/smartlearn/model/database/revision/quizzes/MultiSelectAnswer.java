package com.charles.smartlearn.model.database.revision.quizzes;

public class MultiSelectAnswer extends Answer
{

    public static final String ANSWER_TYPE_TAG = "Multi-Select";

    private String feedbackForSelect;
    private String feedbackForNotSelect;

    public MultiSelectAnswer(String answer, boolean correct, String feedbackForSelect, String feedbackForNotSelect)
    {
        super(ANSWER_TYPE_TAG, answer, correct);
        this.feedbackForSelect = feedbackForSelect;
        this.feedbackForNotSelect = feedbackForNotSelect;
    }

    public MultiSelectAnswer()
    {
        super(ANSWER_TYPE_TAG);
    }

    public String getFeedbackForSelect()
    {
        return feedbackForSelect;
    }

    public String getFeedbackForNotSelect()
    {
        return feedbackForNotSelect;
    }

}

