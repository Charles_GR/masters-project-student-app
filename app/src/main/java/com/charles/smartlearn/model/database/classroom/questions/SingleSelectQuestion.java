package com.charles.smartlearn.model.database.classroom.questions;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.charles.smartlearn.processing.database.LoginData;
import java.util.*;

public class SingleSelectQuestion extends SelectQuestion<String>
{

    public static final String QUESTION_TYPE_TAG = "Single-Select";

    public SingleSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    @Override
    public boolean addAnswer(String answer, boolean correct)
    {
        if(correct && !containsAnswer(answer))
        {
            for(Answer ans : answers)
            {
                ans.setCorrect(false);
            }
        }
        return super.addAnswer(answer, correct);
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<String> response : responses.values())
        {
            String answer = response.getAnswers();
            answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
        }
        return answerCounts;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        RadioGroup rgAnswers = new RadioGroup(context);
        rgAnswers.setTag("rgAnswers");
        for(int i = 0; i < getAnswers().size(); i++)
        {
            RadioButton rbAnswer = new RadioButton(context);
            rbAnswer.setText(getAnswers().get(i).getAnswer());
            rbAnswer.setId(i + 1);
            rgAnswers.addView(rbAnswer);
        }
        return Arrays.asList(new View[]{rgAnswers});
    }

    @Override
    public String findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        for(int i = 0; i < rgAnswers.getChildCount(); i++)
        {
            RadioButton rbAnswer = (RadioButton)rgAnswers.getChildAt(i);
            if(rbAnswer.isChecked())
            {
                return rbAnswer.getText().toString();
            }
        }
        return null;
    }

    @Override
    protected String writeOwnAnswers()
    {
        Response<String> response = responses.get(LoginData.getUsername());
        return response == null ? "" : "The answer you gave is " + response.getAnswers() + ".";
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswer)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        RadioButton rbSelectedAnswer = (RadioButton)rgAnswers.findViewById(rgAnswers.getCheckedRadioButtonId());
        if(rbSelectedAnswer == null)
        {
            return 0;
        }
        for(Answer answer : answers)
        {
            if(rbSelectedAnswer.getText().toString().equals(answer.getAnswer()))
            {
                return answer.getCorrect() ? 1 : 0;
            }
        }
        return 0;
    }

}
