package com.charles.smartlearn.model.adapters.classroom;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.adapters.FilterAdapter;
import com.charles.smartlearn.model.database.classroom.questions.Question;
import com.charles.smartlearn.processing.database.LoginData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class QuestionAdapter extends FilterAdapter<Question>
{

    public QuestionAdapter(Context context, List<Question> questions)
    {
        super(context, R.layout.list_item_class_question, questions);
        setComparator(new ClassroomQuestionComparator());
    }

    public QuestionAdapter(Context context)
    {
        super(context, R.layout.list_item_class_question, new ArrayList<Question>());
        setComparator(new ClassroomQuestionComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_class_question, parent, false);
        }
        Question question = getItem(position);
        TextView tvQuestion = (TextView)convertView.findViewById(R.id.tvQuestionValue);
        TextView tvDateCreated = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        TextView tvStatus = (TextView)convertView.findViewById(R.id.tvStatusValue);
        tvQuestion.setText(question.getQuestion());
        tvDateCreated.setText(SimpleDateFormat.getDateTimeInstance().format(question.getDateCreated()));
        tvStatus.setText(question.getResponses().containsKey(LoginData.getUsername()) ? "Answered" : "Unanswered");
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Question question : items)
        {
            switch(searchField)
            {
                case "Question":
                {
                    if(question.getQuestion().contains(searchQuery))
                    {
                        filteredItems.add(question);
                    }
                    break;
                }
                case "Status":
                    String status = question.getResponses().containsKey(LoginData.getUsername()) ? "Answered" : "Unanswered";
                    if(status.contains(searchQuery))
                    {
                        filteredItems.add(question);
                    }
                    break;
            }
        }
    }

    protected class ClassroomQuestionComparator extends ItemComparator
    {

        @Override
        public int compare(Question questionA, Question questionB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Question":
                    compare = questionA.getQuestion().compareTo(questionB.getQuestion());
                    break;
                case "Date Created":
                    compare = questionA.getDateCreated().compareTo(questionB.getDateCreated());
                    break;
                case "Status":
                    String statusA = questionA.getResponses().containsKey(LoginData.getUsername()) ? "Answered" : "Unanswered";
                    String statusB = questionB.getResponses().containsKey(LoginData.getUsername()) ? "Answered" : "Unanswered";
                    compare = statusA.compareTo(statusB);
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}

