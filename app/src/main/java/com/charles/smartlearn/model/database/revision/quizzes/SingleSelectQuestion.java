package com.charles.smartlearn.model.database.revision.quizzes;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.charles.smartlearn.model.quiz.QuestionResult;
import java.util.*;

public class SingleSelectQuestion extends SelectQuestion<String>
{

    public static final String QUESTION_TYPE_TAG = "Single-Select";

    public SingleSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                String answer = response.getAnswers().get(questionIndex).toString();
                if(answer != null)
                {
                    answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        RadioGroup rgAnswers = new RadioGroup(context);
        rgAnswers.setTag("rgAnswers");
        for(int i = 0; i < getAnswers().size(); i++)
        {
            RadioButton rbAnswer = new RadioButton(context);
            rbAnswer.setText(getAnswers().get(i).getAnswer());
            rbAnswer.setId(i + 1);
            rgAnswers.addView(rbAnswer);
        }
        return Arrays.asList(new View[]{rgAnswers});
    }

    @Override
    public String findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        for(int i = 0; i < rgAnswers.getChildCount(); i++)
        {
            RadioButton rbAnswer = (RadioButton)rgAnswers.getChildAt(i);
            if(rbAnswer.isChecked())
            {
                return rbAnswer.getText().toString();
            }
        }
        return null;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswer)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        RadioButton rbSelectedAnswer = (RadioButton)rgAnswers.findViewById(rgAnswers.getCheckedRadioButtonId());
        if(rbSelectedAnswer == null)
        {
            return 0;
        }
        for(Answer answer : answers)
        {
            if(rbSelectedAnswer.getText().toString().equals(answer.getAnswer()))
            {
                return answer.getCorrect() ? 1 : 0;
            }
        }
        return 0;
    }

    @Override
    public QuestionResult getQuestionResult(LinearLayout llSelectAnswer)
    {
        QuestionResult questionResult = new QuestionResult(question);
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        RadioButton rbSelectedAnswer = (RadioButton)rgAnswers.findViewById(rgAnswers.getCheckedRadioButtonId());
        if(rbSelectedAnswer != null)
        {
            for(Answer answer : answers)
            {
                SingleSelectAnswer singleSelectAnswer = (SingleSelectAnswer)answer;
                if(rbSelectedAnswer.getText().toString().equals(singleSelectAnswer.getAnswer()))
                {
                    questionResult.addFeedback(answer.getAnswer(), true, answer.getCorrect(), singleSelectAnswer.getFeedback());
                    break;
                }
            }
        }
        return questionResult;
    }

}
