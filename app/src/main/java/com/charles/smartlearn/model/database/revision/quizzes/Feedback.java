package com.charles.smartlearn.model.database.revision.quizzes;

public class Feedback
{

    private String answer;
    private String feedback;

    public Feedback(String answer, String feedback)
    {
        this.answer = answer;
        this.feedback = feedback;
    }

    public Feedback()
    {

    }

    public String getAnswer()
    {
        return answer;
    }

    public String getFeedback()
    {
        return feedback;
    }

}
