package com.charles.smartlearn.model.database.revision.quizzes;

import android.content.Context;
import android.text.InputFilter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.quiz.QuestionResult;
import java.util.*;

public class ShortAnswerQuestion extends Question<String>
{

    public static final String QUESTION_TYPE_TAG = "Short Answer";

    private String correctAnswer;

    private HashMap<String, Feedback> feedback;

    public ShortAnswerQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public String getCorrectAnswer()
    {
        return correctAnswer;
    }

    public HashMap<String, Feedback> getFeedback()
    {
        return feedback;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                String answer = (String)response.getAnswers().get(questionIndex);
                if(answer != null)
                {
                    answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        EditText etAnswer = new EditText(context);
        etAnswer.setTag("etAnswer");
        etAnswer.setMinimumWidth(Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, LearnApplication.getInstance().getResources().getDisplayMetrics())));
        etAnswer.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        etAnswer.setGravity(Gravity.CENTER_HORIZONTAL);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        etAnswer.setLayoutParams(layoutParams);
        return Arrays.asList(new View[]{etAnswer});
    }

    @Override
    public String findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        EditText etAnswer = (EditText)llSelectAnswer.findViewWithTag("etAnswer");
        return etAnswer.getText().toString().isEmpty() ? null : etAnswer.getText().toString();
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswer)
    {
        EditText etAnswer = (EditText)llSelectAnswer.findViewWithTag("etAnswer");
        return etAnswer.getText().toString().toLowerCase().equals(correctAnswer.toLowerCase()) ? 1 : 0;
    }

    @Override
    public QuestionResult getQuestionResult(LinearLayout llSelectAnswer)
    {
        QuestionResult questionResult = new QuestionResult(question);
        EditText etAnswer = (EditText)llSelectAnswer.findViewWithTag("etAnswer");
        String answer = etAnswer.getText().toString();
        if(feedback != null && feedback.containsKey(answer))
        {
            questionResult.addFeedback(answer, true, answer.equals(correctAnswer), feedback.get(answer).getFeedback());
        }
        return questionResult;
    }

}
