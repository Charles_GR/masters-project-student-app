package com.charles.smartlearn.model.database.revision.quizzes;

import java.util.*;

public class Quiz
{

    private String key;
    private String title;
    private List<Question> questions;
    private Date dateCreated;
    private HashMap<String, Response> responses;

    public Quiz()
    {
        questions = new ArrayList<>();
        dateCreated = new Date();
        responses = new HashMap<>();
    }

    public String getKey()
    {
        return key;
    }

    public String getTitle()
    {
        return title;
    }

    public List<Question> getQuestions()
    {
        return questions;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public HashMap<String, Response> getResponses()
    {
        return responses;
    }

    public int calcPointsPossible()
    {
        int pointsPossible = 0;
        for(Question question : questions)
        {
            pointsPossible += question.calcPointsPossible();
        }
        return pointsPossible;
    }

    public String writeBestScore()
    {
        List<Response> responsesValues = new ArrayList<>(responses.values());
        Collections.sort(responsesValues);
        return calcFormattedPercentage(responsesValues.get(responsesValues.size() - 1).getPoints(), calcPointsPossible());
    }

    public String writeWorstScore()
    {
        List<Response> responsesValues = new ArrayList<>(responses.values());
        Collections.sort(responsesValues);
        return calcFormattedPercentage(responsesValues.get(0).getPoints(), calcPointsPossible());
    }

    public String writeAverageScore()
    {
        float totalScore = 0;
        for(Response response : responses.values())
        {
            totalScore += response.getPoints();
        }
        float averageScore = totalScore / responses.size();
        return calcFormattedPercentage(averageScore, calcPointsPossible());
    }

    private static String calcFormattedPercentage(float part, int whole)
    {
        if(whole == 0)
        {
            return "";
        }
        float percentage = Math.round(1000f * part / whole) / 10f;
        String percentageText = (percentage == (long)percentage) ? String.format(Locale.UK, "%d", (long)percentage) + "%"
                : String.format("%s", percentage) + "%";
        return part + " / " + whole + " = " + percentageText;
    }

    private static String calcFormattedPercentage(int part, int whole)
    {
        if(whole == 0)
        {
            return "";
        }
        float percentage = Math.round(1000f * part / whole) / 10f;
        String percentageText = (percentage == (long)percentage) ? String.format(Locale.UK, "%d", (long)percentage) + "%"
                : String.format("%s", percentage) + "%";
        return part + " / " + whole + " = " + percentageText;
    }

}
