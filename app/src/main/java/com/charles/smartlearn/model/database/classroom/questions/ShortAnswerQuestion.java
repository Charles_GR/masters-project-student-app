package com.charles.smartlearn.model.database.classroom.questions;

import android.content.Context;
import android.text.InputFilter;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.processing.database.LoginData;
import java.util.*;

public class ShortAnswerQuestion extends Question<String>
{

    public static final String QUESTION_TYPE_TAG = "Short Answer";

    private String correctAnswer;

    public ShortAnswerQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public String getCorrectAnswer()
    {
        return correctAnswer;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<String> response : responses.values())
        {
            String answer = response.getAnswers();
            answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
        }
        return answerCounts;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        EditText etAnswer = new EditText(context);
        etAnswer.setTag("etAnswer");
        etAnswer.setMinimumWidth(Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, LearnApplication.getInstance().getResources().getDisplayMetrics())));
        etAnswer.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        etAnswer.setGravity(Gravity.CENTER_HORIZONTAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        etAnswer.setLayoutParams(layoutParams);
        return Arrays.asList(new View[]{etAnswer});
    }

    @Override
    public String findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        EditText etAnswer = (EditText)llSelectAnswer.findViewWithTag("etAnswer");
        return etAnswer.getText().toString().isEmpty() ? null : etAnswer.getText().toString();
    }

    @Override
    protected String writeOwnAnswers()
    {
        Response<String> response = responses.get(LoginData.getUsername());
        return response == null ? "" : "The answer you gave is " + response.getAnswers() + ".";
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswer)
    {
        EditText etAnswer = (EditText)llSelectAnswer.findViewWithTag("etAnswer");
        return etAnswer.getText().toString().toLowerCase().equals(correctAnswer.toLowerCase()) ? 1 : 0;
    }

}