package com.charles.smartlearn.model.database.classroom.questions;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.charles.smartlearn.processing.database.LoginData;
import java.util.*;

public class MultiSelectQuestion extends SelectQuestion<List<String>>
{

    public static final String QUESTION_TYPE_TAG = "Multi-Select";

    public MultiSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<List<String>> response : responses.values())
        {
            for(String answer : response.getAnswers())
            {
                answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
            }
        }
        return answerCounts;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        List<View> answerViews = new ArrayList<>();
        for(int i = 0; i < getAnswers().size(); i++)
        {
            CheckBox chkAnswer = new CheckBox(context);
            chkAnswer.setText(answers.get(i).getAnswer());
            answerViews.add(chkAnswer);
        }
        return answerViews;
    }

    @Override
    public List<String> findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        List<String> selectedAnswers = new ArrayList<>();
        for(int i = 0; i < llSelectAnswer.getChildCount(); i++)
        {
            CheckBox chkAnswer = (CheckBox)llSelectAnswer.getChildAt(i);
            if(chkAnswer.isChecked())
            {
                selectedAnswers.add(chkAnswer.getText().toString());
            }
        }
        return selectedAnswers.isEmpty() ? null : selectedAnswers;
    }

    @Override
    protected String writeOwnAnswers()
    {
        Response<List<String>> response = responses.get(LoginData.getUsername());
        if(response == null)
        {
            return "";
        }
        String text = "The answers you gave are ";
        for(int i = 0; i < response.getAnswers().size(); i++)
        {
            text += response.getAnswers().get(i);
            if(i + 2 < answers.size())
            {
                text += ", ";
            }
            else if(i + 2 == response.getAnswers().size())
            {
                text += " and ";
            }
        }
        text += ".";
        return text;
    }

    @Override
    public int calcPointsPossible()
    {
        int pointsPossible = 0;
        for(Answer answer : answers)
        {
            if(answer.getCorrect())
            {
                pointsPossible++;
            }
        }
        return pointsPossible;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswer)
    {
        int points = 0;
        for(int i = 0; i < llSelectAnswer.getChildCount(); i++)
        {
            CheckBox chkAnswer = (CheckBox)llSelectAnswer.getChildAt(i);
            if(chkAnswer.isChecked())
            {
                for(Answer answer : answers)
                {
                    if(chkAnswer.getText().toString().equals(answer.getAnswer()))
                    {
                        points += answer.getCorrect() ? 1 : -1;
                        break;
                    }
                }
            }
        }
        return Math.max(0, points);
    }

}