package com.charles.smartlearn.model.database.revision.quizzes;

public class SingleSelectAnswer extends Answer
{

    public static final String ANSWER_TYPE_TAG = "Single-Select";

    private String feedback;

    public SingleSelectAnswer(String answer, boolean correct, String feedback)
    {
        super(ANSWER_TYPE_TAG, answer, correct);
        this.feedback = feedback;
    }

    public SingleSelectAnswer()
    {
        super(ANSWER_TYPE_TAG);
    }

    public String getFeedback()
    {
        return feedback;
    }

}
