package com.charles.smartlearn.model.database.revision.quizzes;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import com.charles.smartlearn.model.quiz.QuestionResult;
import java.util.*;

public class MultiSelectQuestion extends SelectQuestion<List<String>>
{

    public static final String QUESTION_TYPE_TAG = "Multi-Select";

    public MultiSelectQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    @SuppressWarnings("unchecked")
    @Override
    public HashMap<String, Integer> calcAnswerCounts(HashMap<String, Response> responses, int questionIndex)
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response response : responses.values())
        {
            if(questionIndex < response.getAnswers().size())
            {
                List<String> answers = (List<String>) response.getAnswers().get(questionIndex);
                if(answers != null)
                {
                    for(String answer : answers)
                    {
                        answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
                    }
                }
            }
        }
        return answerCounts;
    }

    @Override
    public int calcPointsPossible()
    {
        int pointsPossible = 0;
        for(Answer answer : answers)
        {
            if(answer.getCorrect())
            {
                pointsPossible++;
            }
        }
        return pointsPossible;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        List<View> answerViews = new ArrayList<>();
        for(int i = 0; i < getAnswers().size(); i++)
        {
            CheckBox chkAnswer = new CheckBox(context);
            chkAnswer.setTag("chkAnswer");
            chkAnswer.setText(answers.get(i).getAnswer());
            answerViews.add(chkAnswer);
        }
        return answerViews;
    }

    @Override
    public List<String> findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        List<String> selectedAnswers = new ArrayList<>();
        for(int i = 0; i < llSelectAnswer.getChildCount(); i++)
        {
            CheckBox chkAnswer = (CheckBox)llSelectAnswer.getChildAt(i);
            if(chkAnswer.isChecked())
            {
                selectedAnswers.add(chkAnswer.getText().toString());
            }
        }
        return selectedAnswers.isEmpty() ? null : selectedAnswers;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswer)
    {
        int points = 0;
        for(int i = 0; i < llSelectAnswer.getChildCount(); i++)
        {
            CheckBox chkAnswer = (CheckBox)llSelectAnswer.getChildAt(i);
            if(chkAnswer.isChecked())
            {
                for(Answer answer : answers)
                {
                    if(chkAnswer.getText().toString().equals(answer.getAnswer()))
                    {
                        points += answer.getCorrect() ? 1 : -1;
                        break;
                    }
                }
            }
        }
        return Math.max(0, points);
    }

    @Override
    public QuestionResult getQuestionResult(LinearLayout llSelectAnswer)
    {
        QuestionResult questionResult = new QuestionResult(question);
        for(int i = 0; i < llSelectAnswer.getChildCount(); i++)
        {
            CheckBox chkAnswer = (CheckBox)llSelectAnswer.getChildAt(i);
            for(Answer answer : answers)
            {
                MultiSelectAnswer multiSelectAnswer = (MultiSelectAnswer)answer;
                if(chkAnswer.getText().toString().equals(multiSelectAnswer.getAnswer()))
                {
                    String feedback = chkAnswer.isChecked() ? multiSelectAnswer.getFeedbackForSelect() : multiSelectAnswer.getFeedbackForNotSelect();
                    questionResult.addFeedback(answer.getAnswer(), chkAnswer.isChecked(), answer.getCorrect(), feedback);
                }
            }
        }
        return questionResult;
    }

}
