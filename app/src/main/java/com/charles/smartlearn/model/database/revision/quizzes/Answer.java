package com.charles.smartlearn.model.database.revision.quizzes;

import com.shaded.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = SingleSelectAnswer.class, name = SingleSelectAnswer.ANSWER_TYPE_TAG),
               @JsonSubTypes.Type(value = MultiSelectAnswer.class, name = MultiSelectAnswer.ANSWER_TYPE_TAG)})
public abstract class Answer
{

    protected String answer;
    protected boolean correct;
    protected String type;

    public Answer(String type, String answer, boolean correct)
    {
        this.answer = answer;
        this.correct = correct;
        this.type = type;
    }

    public Answer(String type)
    {
        this.type = type;
    }

    public String getAnswer()
    {
        return answer;
    }

    public boolean getCorrect()
    {
        return correct;
    }

    public String getType()
    {
        return type;
    }

    @Override
    public String toString()
    {
        return answer;
    }

}
