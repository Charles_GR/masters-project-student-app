package com.charles.smartlearn.model.database.revision.quizzes;

import java.util.ArrayList;
import java.util.List;

public class Response implements Comparable<Response>
{

    private String student;
    private int points;
    private List answers;

    public Response(String student)
    {
        this.student = student;
        points = 0;
        answers = new ArrayList();
    }

    public Response()
    {
        answers = new ArrayList();
    }

    public String getStudent()
    {
        return student;
    }

    public int getPoints()
    {
        return points;
    }

    public List getAnswers()
    {
        return answers;
    }

    public void setPoints(int points)
    {
        this.points = points;
    }

    @Override
    public int compareTo(Response otherResponse)
    {
        return Integer.valueOf(points).compareTo(otherResponse.getPoints());
    }

}
