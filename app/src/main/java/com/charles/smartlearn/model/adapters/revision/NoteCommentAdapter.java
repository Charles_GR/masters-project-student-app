package com.charles.smartlearn.model.adapters.revision;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.adapters.FilterAdapter;
import com.charles.smartlearn.model.database.revision.notes.Comment;
import java.util.ArrayList;
import java.util.List;

public class NoteCommentAdapter extends FilterAdapter<Comment>
{

    public NoteCommentAdapter(Context context, List<Comment> comments)
    {
        super(context, R.layout.list_item_note_comment, comments);
        setComparator(new NoteCommentComparator());
    }

    public NoteCommentAdapter(Context context)
    {
        super(context, R.layout.list_item_note_comment, new ArrayList<Comment>());
        setComparator(new NoteCommentComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_note_comment, parent, false);
        }
        Comment comment = getItem(position);
        TextView tvComment = (TextView)convertView.findViewById(R.id.tvComment);
        tvComment.setText(getContext().getString(R.string.comment_text, comment.getAuthor(), comment.getComment()));
        return convertView;
    }

    @Override
    protected void performSearch(String searchField, String searchQuery)
    {
        for(Comment comment : items)
        {
            switch(searchField)
            {
                case "Comment":
                    if(comment.getComment().contains(searchQuery))
                    {
                        filteredItems.add(comment);
                    }
                    break;
                case "Author":
                    if(comment.getAuthor().contains(searchQuery))
                    {
                        filteredItems.add(comment);
                    }
                    break;
            }
        }
    }

    protected class NoteCommentComparator extends ItemComparator
    {

        @Override
        public int compare(Comment commentA, Comment commentB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Comment":
                    compare = commentA.getComment().compareTo(commentB.getComment());
                    break;
                case "Author":
                    compare = commentA.getAuthor().compareTo(commentB.getAuthor());
                    break;
                case "Date Created":
                    compare = commentA.getDateCreated().compareTo(commentB.getDateCreated());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}