package com.charles.smartlearn.model.database.performance;

import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import java.util.*;
import java.util.Map.Entry;

public class Performance implements Comparable<Performance>
{

    private HashMap<String, Score> scores;
    private List<Score> filteredScores;
    private boolean shared;

    public Performance()
    {
        scores = new HashMap<>();
        filteredScores = new ArrayList<>();
        shared = false;
    }

    public HashMap<String, Score> getScores()
    {
        return scores;
    }

    public boolean getShared()
    {
        return shared;
    }

    public void filterByTime(String timePeriod)
    {
        filteredScores.clear();
        for(Score score : scores.values())
        {
            long timeDiff = (new Date().getTime() - score.getDateCreated().getTime()) / 1000;
            boolean inTime = false;
            switch(timePeriod)
            {
                case "All Time":
                    inTime = true;
                    break;
                case "Past Year":
                    inTime = timeDiff < 31536000;
                    break;
                case "Past 6 Months":
                    inTime = timeDiff < 15768000;
                    break;
                case "Past 3 Months":
                    inTime = timeDiff < 7884000;
                    break;
                case "Past Month":
                    inTime = timeDiff < 2628000;
                    break;
                case "Past 2 Weeks":
                    inTime = timeDiff < 1209600;
                    break;
                case "Past Week":
                    inTime = timeDiff < 604800;
                    break;
                case "Past 3 Days":
                    inTime = timeDiff < 259200;
                    break;
                case "Past Day":
                    inTime = timeDiff < 86400;
                    break;
            }
            if(inTime)
            {
                filteredScores.add(score);
            }
        }
    }

    public int calcPointsAwarded()
    {
        int pointsAwarded = 0;
        for(Score score : filteredScores)
        {
            pointsAwarded += score.getPointsAwarded();
        }
        return pointsAwarded;
    }

    public int calcPointsPossible()
    {
        int pointsPossible = 0;
        for(Score score : filteredScores)
        {
            pointsPossible += score.getPointsPossible();
        }
        return pointsPossible;
    }

    public float calcPercentageCorrect()
    {
        return calcPointsPossible() == 0 ? 0 : 100f * calcPointsAwarded() / calcPointsPossible();
    }

    public String calcFormattedPercentageCorrect()
    {
        float percentage = Math.round(10f * calcPercentageCorrect()) / 10f;
        return (percentage == (long)percentage) ? String.format(Locale.UK, "%d", (long)percentage) + "%"
                                                : String.format("%s", percentage) + "%";
    }

    public String writeScore()
    {
        return calcPointsAwarded() + " / " + calcPointsPossible() + " = " + calcFormattedPercentageCorrect();
    }

    public static String writeAverageScore(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> nonEmptyPerformances = getNonEmptyPerformances(performances);
        return nonEmptyPerformances.isEmpty() ? LearnApplication.getInstance().getString(R.string.not_yet_available)
                : calcFormattedAveragePercentageCorrect(nonEmptyPerformances) + " by " + nonEmptyPerformances.size() + " students";
    }

    public static HashMap<String, Performance> getBestPerformances(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> bestStudents = new HashMap<>();
        for(Entry<String, Performance> entry : performances.entrySet())
        {
            if(bestStudents.isEmpty() || entry.getValue().equals(bestStudents.entrySet().iterator().next().getValue()))
            {
                bestStudents.put(entry.getKey(), entry.getValue());
            }
            else if(entry.getValue().compareTo(bestStudents.entrySet().iterator().next().getValue()) < 0)
            {
                bestStudents.clear();
                bestStudents.put(entry.getKey(), entry.getValue());
            }
        }
        return bestStudents;
    }

    public static HashMap<String, Performance> getWorstPerformances(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> worstPerformances = new HashMap<>();
        for(Entry<String, Performance> entry : performances.entrySet())
        {
            if(worstPerformances.isEmpty() || entry.getValue().equals(worstPerformances.entrySet().iterator().next().getValue()))
            {
                worstPerformances.put(entry.getKey(), entry.getValue());
            }
            else if(entry.getValue().compareTo(worstPerformances.entrySet().iterator().next().getValue()) > 0)
            {
                worstPerformances.clear();
                worstPerformances.put(entry.getKey(), entry.getValue());
            }
        }
        return worstPerformances;
    }

    public static String performancesToString(HashMap<String, Performance> performances)
    {
        if(performances.isEmpty())
        {
            return LearnApplication.getInstance().getString(R.string.not_yet_available);
        }
        String perfText = "";
        Iterator<Entry<String, Performance>> iterator = performances.entrySet().iterator();
        while(iterator.hasNext())
        {
            perfText += iterator.next().getKey();
            if(iterator.hasNext())
            {
                perfText += ", ";
            }
        }
        Performance performance = performances.entrySet().iterator().next().getValue();
        perfText += " (" + performance.calcPointsAwarded() + " / " + performance.calcPointsPossible() + " = " +
                performance.calcFormattedPercentageCorrect() + ")";
        return perfText;
    }

    private static String calcFormattedAveragePercentageCorrect(HashMap<String, Performance> performances)
    {
        float averagePercentage = Math.round(10f * calcAveragePercentageCorrect(performances)) / 10f;
        return (averagePercentage == (long)averagePercentage) ? String.format(Locale.UK, "%d", (long)averagePercentage) + "%"
                                                              : String.format("%s", averagePercentage) + "%";
    }

    private static float calcAveragePercentageCorrect(HashMap<String, Performance> performances)
    {
        float totalPercentages = 0;
        for(Performance performance : performances.values())
        {
            totalPercentages += performance.calcPercentageCorrect();
        }
        return totalPercentages / performances.size();
    }

    public static HashMap<String, Performance> getNonEmptyPerformances(HashMap<String, Performance> performances)
    {
        HashMap<String, Performance> nonEmptyPerformances = new HashMap<>();
        for(Entry<String, Performance> performance : performances.entrySet())
        {
            if(performance.getValue().calcPointsPossible() > 0)
            {
                nonEmptyPerformances.put(performance.getKey(), performance.getValue());
            }
        }
        return nonEmptyPerformances;
    }

    @Override
    public int compareTo(Performance otherPerf)
    {
        int pointsPossible = calcPointsPossible();
        int otherPointsPossible = otherPerf.calcPointsPossible();
        int compare = Float.compare(otherPerf.calcPercentageCorrect(), calcPercentageCorrect());
        compare = (compare == 0 && calcPointsAwarded() == 0) ? pointsPossible - otherPointsPossible : compare;
        compare = (compare == 0) ? otherPointsPossible - pointsPossible : compare;
        return compare;
    }

    @Override
    public boolean equals(Object other)
    {
        if(other instanceof Performance)
        {
            Performance perf = (Performance)other;
            return perf.calcPointsAwarded() == calcPointsAwarded() && perf.calcPointsPossible() == calcPointsPossible();
        }
        return false;
    }

}


