package com.charles.smartlearn.model.database.classroom.comments;

import com.charles.smartlearn.processing.database.LoginData;
import java.util.Date;
import java.util.LinkedHashMap;

public class Comment
{

    private String key;
    private String author;
    private Date dateCreated;
    private String comment;
    private String imageKey;
    private LinkedHashMap<String, CommentReply> replies;

    public Comment(String key, String comment, String imageKey)
    {
        this.key = key;
        author = LoginData.getUsername();
        dateCreated = new Date();
        this.comment = comment;
        this.imageKey = imageKey;
        replies = new LinkedHashMap<>();
    }

    public Comment()
    {
        replies = new LinkedHashMap<>();
    }

    public String getKey()
    {
        return key;
    }

    public String getAuthor()
    {
        return author;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public String getComment()
    {
        return comment;
    }

    public String getImageKey()
    {
        return imageKey;
    }

    public LinkedHashMap<String, CommentReply> getReplies()
    {
        return replies;
    }

    @Override
    public String toString()
    {
        return comment;
    }

}

