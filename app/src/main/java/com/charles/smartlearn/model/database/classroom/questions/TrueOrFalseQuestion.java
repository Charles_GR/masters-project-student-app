package com.charles.smartlearn.model.database.classroom.questions;

import android.content.Context;
import android.view.View;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.processing.database.LoginData;
import java.util.*;

public class TrueOrFalseQuestion extends Question<Boolean>
{

    public static final String QUESTION_TYPE_TAG = "True Or False";

    private Boolean correctAnswer;

    public TrueOrFalseQuestion()
    {
        super(QUESTION_TYPE_TAG);
    }

    public Boolean getCorrectAnswer()
    {
        return correctAnswer;
    }

    @Override
    public HashMap<String, Integer> calcAnswerCounts()
    {
        HashMap<String, Integer> answerCounts = new HashMap<>();
        for(Response<Boolean> response : responses.values())
        {
            String answer = response.getAnswers().toString();
            answerCounts.put(answer, answerCounts.containsKey(answer) ? answerCounts.get(answer) + 1 : 1);
        }
        return answerCounts;
    }

    @Override
    public List<View> createAnswerViews(Context context)
    {
        RadioGroup rgAnswers = new RadioGroup(context);
        rgAnswers.setTag("rgAnswers");
        rgAnswers.addView(createAnswerRadioButton(context, context.getString(R.string.ans_true), 1));
        rgAnswers.addView(createAnswerRadioButton(context, context.getString(R.string.ans_false), 2));
        return Arrays.asList(new View[]{rgAnswers});
    }

    @Override
    public Boolean findStudentAnswers(Context context, LinearLayout llSelectAnswer)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswer.findViewWithTag("rgAnswers");
        for(int i = 0; i < rgAnswers.getChildCount(); i++)
        {
            RadioButton rbAnswer = (RadioButton)rgAnswers.getChildAt(i);
            if(rbAnswer.isChecked())
            {
                return Boolean.parseBoolean(rbAnswer.getText().toString());
            }
        }
        return null;
    }

    @Override
    protected String writeOwnAnswers()
    {
        Response<Boolean> response = responses.get(LoginData.getUsername());
        return response == null ? "" : "The answer you gave is " + response.getAnswers() + ".";
    }

    @Override
    public int calcPointsPossible()
    {
        return 1;
    }

    @Override
    public int calcPointsForAnswers(LinearLayout llSelectAnswers)
    {
        RadioGroup rgAnswers = (RadioGroup)llSelectAnswers.findViewWithTag("rgAnswers");
        RadioButton rbSelectedAnswer = (RadioButton)rgAnswers.findViewById(rgAnswers.getCheckedRadioButtonId());
        return (rbSelectedAnswer != null && Boolean.parseBoolean(rbSelectedAnswer.getText().toString()) == correctAnswer) ? 1 : 0;
    }

    private static RadioButton createAnswerRadioButton(Context context, String answer, int id)
    {
        RadioButton rbAnswer = new RadioButton(context);
        rbAnswer.setText(answer);
        rbAnswer.setId(id);
        return rbAnswer;
    }

}

