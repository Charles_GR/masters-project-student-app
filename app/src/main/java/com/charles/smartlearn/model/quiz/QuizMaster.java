package com.charles.smartlearn.model.quiz;

import android.widget.LinearLayout;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.database.performance.Score.ContentType;
import com.charles.smartlearn.model.database.revision.quizzes.*;
import com.charles.smartlearn.processing.database.*;
import java.util.*;

public class QuizMaster
{

    private static QuizMaster instance;

    private Quiz quiz;
    private Question currentQuestion;
    private int currentQuestionIndex;
    private int pointsAwarded;
    private int pointsPossible;
    private int questionsAnswered;
    private LinkedHashMap<String, QuestionResult> questionResults;
    private Response quizResponse;

    private QuizMaster(Quiz quiz)
    {
        this.quiz = quiz;
        currentQuestionIndex = 0;
        pointsAwarded = 0;
        pointsPossible = 0;
        questionsAnswered = 0;
        currentQuestion = quiz.getQuestions().get(currentQuestionIndex);
        questionResults = new LinkedHashMap<>();
        quizResponse = new Response(LoginData.getUsername());
    }

    public static QuizMaster getInstance()
    {
        return instance;
    }

    public static void createNewInstance(Quiz quiz)
    {
        instance = new QuizMaster(quiz);
    }

    public LinkedHashMap<String, QuestionResult> getQuestionResults()
    {
        return questionResults;
    }

    public int getPointsAwarded()
    {
        return pointsAwarded;
    }

    public int getPointsPossible()
    {
        return pointsPossible;
    }

    public int getQuestionsAnswered()
    {
        return questionsAnswered;
    }

    public Question getCurrentQuestion()
    {
        return currentQuestion;
    }

    public boolean hasNextQuestion()
    {
        return currentQuestionIndex < quiz.getQuestions().size() - 1;
    }

    public int getNumberOfQuestions()
    {
        return quiz.getQuestions().size();
    }

    public void goToNextQuestion()
    {
        if(hasNextQuestion())
        {
            currentQuestion = quiz.getQuestions().get(++currentQuestionIndex);
        }
    }

    public void addQuestionAnswered()
    {
        questionsAnswered++;
    }

    public void addToPointsAwarded(LinearLayout llSelectAnswer)
    {
        pointsAwarded += currentQuestion.calcPointsForAnswers(llSelectAnswer);
    }

    public void addToPointsPossible()
    {
        pointsPossible += currentQuestion.calcPointsPossible();
    }

    public void addQuestionResult(LinearLayout llSelectAnswer)
    {
        QuestionResult questionResult = currentQuestion.getQuestionResult(llSelectAnswer);
        if(questionResult != null)
        {
            questionResults.put(currentQuestion.getQuestion(), currentQuestion.getQuestionResult(llSelectAnswer));
        }
    }

    @SuppressWarnings("unchecked")
    public void addQuestionResponse(LinearLayout llSelectAnswer)
    {
        quizResponse.getAnswers().add(currentQuestion.findStudentAnswers(LearnApplication.getInstance(), llSelectAnswer));
    }

    public void saveQuizResponse()
    {
        quizResponse.setPoints(pointsAwarded);
        FirebaseData.getRevisionQuizResponseRef(quiz.getKey(), LoginData.getUsername()).setValue(quizResponse);
    }

    public int getPercentageScore()
    {
        return Math.round(100f * (float)pointsAwarded / (float)pointsPossible);
    }

    public void updatePerformance()
    {
        PerformanceData.updatePerformance(ContentType.REVISION_QUIZ_TYPE, quiz.getKey(), quiz.getQuestions().size(), pointsAwarded, pointsPossible);
    }

}
