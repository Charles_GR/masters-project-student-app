package com.charles.smartlearn.model.quiz;

class AnswerResult
{

    private String answer;
    private boolean selected;
    private boolean correct;
    private String feedback;

    public AnswerResult(String answer, boolean selected, boolean correct, String feedback)
    {
        this.answer = answer;
        this.selected = selected;
        this.correct = correct;
        this.feedback = feedback;
    }

    public String getAnswer()
    {
        return answer;
    }

    public boolean getSelected()
    {
        return selected;
    }

    public boolean getCorrect()
    {
        return correct;
    }

    public String getFeedback()
    {
        return feedback;
    }

}