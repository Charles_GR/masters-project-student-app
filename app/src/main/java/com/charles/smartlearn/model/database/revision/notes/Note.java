package com.charles.smartlearn.model.database.revision.notes;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;
import com.charles.smartlearn.R;
import com.charles.smartlearn.processing.database.LoginData;
import com.charles.smartlearn.processing.media.ImageParser;
import com.charles.smartlearn.processing.media.VideoChecker;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import java.util.*;

public class Note
{

    private String key;
    private String title;
    private String content;
    private String author;
    private Date dateCreated;
    private HashMap<String, Comment> comments;

    public Note(String key, String title, String content)
    {
        this.key = key;
        this.title = title;
        this.content = content;
        this.author = LoginData.getUsername();
        this.dateCreated = new Date();
        this.comments = new HashMap<>();
    }

    public Note()
    {

    }

    public String getKey()
    {
        return key;
    }

    public String getTitle()
    {
        return title;
    }

    public String getContent()
    {
        return content;
    }

    public String getAuthor()
    {
        return author;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public HashMap<String, Comment> getComments()
    {
        return comments;
    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public void createNoteViews(Activity activity, Map<String, String> images, TextView tvNoteTitle, LinearLayout llNoteContent)
    {
        tvNoteTitle.setText(activity.getString(R.string.note_title, title, author));
        llNoteContent.removeAllViews();
        for(String part : content.split("#"))
        {
            if(part.trim().startsWith("<image>") && part.trim().endsWith("</image>"))
            {
                createImageView(activity, images, part.trim().replaceAll("<image>", "").replaceAll("</image>", ""), llNoteContent);
            }
            else if(part.trim().startsWith("<video>") && part.trim().endsWith("</video>"))
            {
                createVideoView(activity, part.trim().replaceAll("<video>", "").replaceAll("</video>", ""), llNoteContent);
            }
            else if(!part.trim().isEmpty())
            {
                createTextView(activity, part.trim(), llNoteContent);
            }
        }
    }

    private void createImageView(Activity activity, Map<String, String> images, String key, LinearLayout llNoteContent)
    {
        ImageView ivImage = new ImageView(activity);
        LayoutParams imageParams;
        if(images.containsKey(key))
        {
            ivImage.setImageBitmap(ImageParser.decodeImage(images.get(key)));
            imageParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        }
        else
        {
            ivImage.setImageDrawable(ContextCompat.getDrawable(activity, R.mipmap.no_image));
            imageParams = new LayoutParams(500, 500);
        }
        imageParams.setMargins(0, 15, 0, 15);
        imageParams.gravity = Gravity.CENTER_HORIZONTAL;
        llNoteContent.addView(ivImage);
        ivImage.setLayoutParams(imageParams);
    }

    private void createVideoView(final Activity activity, final String videoID, LinearLayout llNoteContent)
    {
        ImageView ivVideo = new ImageView(activity);
        llNoteContent.addView(ivVideo);
        ivVideo.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.video));
        LayoutParams imageParams = new LayoutParams(1000, 700);
        imageParams.setMargins(0, 15, 0, 15);
        imageParams.gravity = Gravity.CENTER_HORIZONTAL;
        ivVideo.setLayoutParams(imageParams);
        ivVideo.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent(activity, VideoChecker.YOUTUBE_API_KEY, videoID, 0, true, false);
                activity.startActivity(intent);
            }
        });
    }

    private void createTextView(Activity activity, String text, LinearLayout llNoteContent)
    {
        TextView tvText = new TextView(activity);
        tvText.setText(text);
        llNoteContent.addView(tvText);
        LayoutParams textParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        textParams.setMargins(0, 15, 0, 15);
        tvText.setLayoutParams(textParams);
    }

}