package com.charles.smartlearn.model.database.revision.notes;

import com.charles.smartlearn.processing.database.LoginData;
import java.util.Date;

public class Comment
{

    private String key;
    private String author;
    private Date dateCreated;
    private String comment;

    public Comment(String key, String comment)
    {
        this.key = key;
        author = LoginData.getUsername();
        dateCreated = new Date();
        this.comment = comment;
    }

    public Comment()
    {

    }

    public String getKey()
    {
        return key;
    }

    public String getAuthor()
    {
        return author;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    public String getComment()
    {
        return comment;
    }

    @Override
    public String toString()
    {
        return comment;
    }

}
