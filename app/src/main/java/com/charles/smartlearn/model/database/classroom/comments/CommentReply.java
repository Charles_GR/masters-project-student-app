package com.charles.smartlearn.model.database.classroom.comments;

import com.charles.smartlearn.processing.database.LoginData;

public class CommentReply
{

    private String key;
    private String commentKey;
    private String author;
    private String reply;

    public CommentReply(String key, String commentKey, String reply)
    {
        this.key = key;
        this.commentKey = commentKey;
        author = LoginData.getUsername();
        this.reply = reply;
    }

    public CommentReply()
    {

    }

    public String getKey()
    {
        return key;
    }

    public String getCommentKey()
    {
        return commentKey;
    }

    public String getAuthor()
    {
        return author;
    }

    public String getReply()
    {
        return reply;
    }

}
