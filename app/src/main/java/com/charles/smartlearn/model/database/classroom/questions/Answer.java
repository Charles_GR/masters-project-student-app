package com.charles.smartlearn.model.database.classroom.questions;

public class Answer
{

    private String answer;
    private boolean correct;

    public Answer(String answer, boolean correct)
    {
        this.answer = answer;
        this.correct = correct;
    }

    public Answer()
    {

    }

    public String getAnswer()
    {
        return answer;
    }

    public boolean getCorrect()
    {
        return correct;
    }

    public void setCorrect(boolean correct)
    {
        this.correct = correct;
    }

    @Override
    public String toString()
    {
        return answer;
    }

}
