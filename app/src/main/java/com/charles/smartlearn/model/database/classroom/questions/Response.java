package com.charles.smartlearn.model.database.classroom.questions;

public class Response<AnswersType>
{

    private String student;
    private AnswersType answers;

    public Response(String student, AnswersType answers)
    {
        this.student = student;
        this.answers = answers;
    }

    public Response()
    {

    }

    public String getStudent()
    {
        return student;
    }

    public AnswersType getAnswers()
    {
        return answers;
    }

}
