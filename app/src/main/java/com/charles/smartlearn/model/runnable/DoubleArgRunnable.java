package com.charles.smartlearn.model.runnable;

public interface DoubleArgRunnable<Ta, Tb>
{

    void run(Ta argA, Tb argB);

}
