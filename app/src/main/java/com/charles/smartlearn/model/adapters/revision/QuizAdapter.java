package com.charles.smartlearn.model.adapters.revision;

import android.content.Context;
import android.view.*;
import android.widget.TextView;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.adapters.FilterAdapter;
import com.charles.smartlearn.model.database.performance.Score;
import com.charles.smartlearn.model.database.revision.quizzes.Quiz;
import com.charles.smartlearn.model.runnable.DoubleArgRunnable;
import com.charles.smartlearn.processing.database.PerformanceData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class QuizAdapter extends FilterAdapter<Quiz>
{

    public QuizAdapter(Context context, List<Quiz> quizzes)
    {
        super(context, R.layout.list_item_quiz, quizzes);
        setComparator(new QuizComparator());
    }

    public QuizAdapter(Context context)
    {
        super(context, R.layout.list_item_quiz, new ArrayList<Quiz>());
        setComparator(new QuizComparator());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_quiz, parent, false);
        }
        Quiz quiz = getItem(position);
        TextView tvTitleValue = (TextView)convertView.findViewById(R.id.tvTitleValue);
        TextView tvDateCreatedValue = (TextView)convertView.findViewById(R.id.tvDateCreatedValue);
        TextView tvYourCurrentScoreValue = (TextView)convertView.findViewById(R.id.tvYourCurrentScoreValue);
        tvTitleValue.setText(quiz.getTitle());
        tvDateCreatedValue.setText(SimpleDateFormat.getDateTimeInstance().format(quiz.getDateCreated()));
        PerformanceData.getQuizPerformance(quiz.getKey(), tvYourCurrentScoreValue, getYourQuizScoreOnSuccess, getYourQuizScoreOnError);
        return convertView;
    }

    public void performSearch(String searchField, String searchQuery)
    {
        for(Quiz quiz : items)
        {
            if(searchField.equals("Title"))
            {
                if(quiz.getTitle().contains(searchQuery))
                {
                    filteredItems.add(quiz);
                }
            }
        }
    }

    protected class QuizComparator extends ItemComparator
    {

        @Override
        public int compare(Quiz quizA, Quiz quizB)
        {
            int compare = 0;
            if(sortField.equals("Title"))
            {
                compare = quizA.getTitle().compareTo(quizB.getTitle());
            }
            if(sortField.equals("Date Created"))
            {
                compare = quizA.getDateCreated().compareTo(quizB.getDateCreated());
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

    private DoubleArgRunnable<Score, TextView> getYourQuizScoreOnSuccess = new DoubleArgRunnable<Score, TextView>()
    {
        @Override
        public void run(Score score, TextView textView)
        {
            textView.setText(score == null ? getContext().getString(R.string.not_yet_available) : score.toString());
        }
    };

    private DoubleArgRunnable<String, TextView> getYourQuizScoreOnError = new DoubleArgRunnable<String, TextView>()
    {
        @Override
        public void run(String text, TextView textView)
        {
            textView.setText(text);
        }
    };

}
