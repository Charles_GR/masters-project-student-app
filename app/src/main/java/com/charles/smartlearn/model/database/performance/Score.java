package com.charles.smartlearn.model.database.performance;

import java.util.Date;

public class Score
{

    public enum ContentType
    {
        CLASSROOM_QUESTION_TYPE,
        REVISION_QUIZ_TYPE
    }

    private ContentType contentType;
    private String contentKey;
    private int questionCount;
    private int pointsAwarded;
    private int pointsPossible;
    private Date dateCreated;

    public Score(ContentType contentType, String contentKey, int questionCount, int pointsAwarded, int pointsPossible)
    {
        this.contentType = contentType;
        this.contentKey = contentKey;
        this.questionCount = questionCount;
        this.pointsAwarded = pointsAwarded;
        this.pointsPossible = pointsPossible;
        dateCreated = new Date();
    }

    public Score()
    {

    }

    public ContentType getContentType()
    {
        return contentType;
    }

    public String getContentKey()
    {
        return contentKey;
    }

    public int getQuestionCount()
    {
        return questionCount;
    }

    public int getPointsAwarded()
    {
        return pointsAwarded;
    }

    public int getPointsPossible()
    {
        return pointsPossible;
    }

    public Date getDateCreated()
    {
        return dateCreated;
    }

    @Override
    public String toString()
    {
        return pointsAwarded + " / " + pointsPossible;
    }

}
