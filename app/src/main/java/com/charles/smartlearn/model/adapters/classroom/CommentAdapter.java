package com.charles.smartlearn.model.adapters.classroom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.database.classroom.comments.Comment;
import com.charles.smartlearn.model.database.classroom.comments.CommentReply;
import com.charles.smartlearn.model.runnable.DoubleArgRunnable;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.charles.smartlearn.processing.database.*;
import com.charles.smartlearn.view.extras.PopupDialogs;
import java.util.*;

public class CommentAdapter extends BaseExpandableListAdapter
{

    private Context context;
    private List<Comment> comments;
    private List<Comment> filteredComments;
    private ClassroomCommentComparator comparator;
    private CommentReply selectedCommentReply;

    public CommentAdapter(Context context, List<Comment> comments)
    {
        this.context = context;
        this.comments = comments;
        filteredComments = new ArrayList<>(comments);
        comparator = new ClassroomCommentComparator();
    }

    public CommentAdapter(Context context)
    {
        this.context = context;
        this.comments = new ArrayList<>();
        filteredComments = new ArrayList<>();
        comparator = new ClassroomCommentComparator();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_class_comment_child, parent, false);
        }
        Comment comment = (Comment)getChild(groupPosition, childPosition);
        Button btnAddReply = (Button)convertView.findViewById(R.id.btnAddReply);
        Button btnDeleteReply = (Button)convertView.findViewById(R.id.btnDeleteReply);
        ImageView ivImage = (ImageView) convertView.findViewById(R.id.ivImage);
        LinearLayout llReplies = (LinearLayout)convertView.findViewById(R.id.llReplies);
        TextView tvReplies = (TextView)convertView.findViewById(R.id.tvReplies);
        TextView tvNoRepliesYet = (TextView)convertView.findViewById(R.id.tvNoRepliesYet);
        TextView tvLoadImageErrorMessage = (TextView)convertView.findViewById(R.id.tvLoadImageErrorMessage);
        tvNoRepliesYet.setVisibility(comment.getReplies().isEmpty() ? View.VISIBLE : View.GONE);
        tvLoadImageErrorMessage.setVisibility(View.GONE);
        ivImage.setImageDrawable(null);
        if(comment.getImageKey() != null)
        {
            ClassroomData.getImage(comment.getImageKey(), ivImage, tvLoadImageErrorMessage, getImageOnSuccess, getImageOnError);
        }
        displayReplies(comment, llReplies);
        CommentActionsOnClickListener commentActionsOnClickListener = new CommentActionsOnClickListener(comment, tvReplies);
        btnAddReply.setOnClickListener(commentActionsOnClickListener);
        btnDeleteReply.setOnClickListener(commentActionsOnClickListener);
        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_class_comment_group, parent, false);
        }
        Comment comment = (Comment)getGroup(groupPosition);
        ImageView ivIndicator = (ImageView)convertView.findViewById(R.id.ivIndicator);
        TextView tvComment = (TextView)convertView.findViewById(R.id.tvComment);
        ivIndicator.setImageDrawable(ContextCompat.getDrawable(context, isExpanded ? android.R.drawable.arrow_up_float : android.R.drawable.arrow_down_float));
        tvComment.setText(context.getString(R.string.comment_text, comment.getAuthor(), comment.getComment()));
        return convertView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition)
    {
        return filteredComments.get(groupPosition);
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return filteredComments.get(groupPosition);
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        return 1;
    }

    @Override
    public int getGroupCount()
    {
        return filteredComments.size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return 0;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return false;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    public void sortComments(String sortField, String sortOrdering)
    {
        comparator.setSortField(sortField);
        comparator.setSortOrdering(sortOrdering);
        Collections.sort(filteredComments, comparator);
        notifyDataSetChanged();
    }

    public void searchComments(String searchField, String searchQuery)
    {
        filteredComments.clear();
        if(searchQuery.isEmpty())
        {
            filteredComments.addAll(comments);
        }
        else
        {
            for(Comment comment : comments)
            {
                switch(searchField)
                {
                    case "Comment":
                        if(comment.getComment().contains(searchQuery))
                        {
                            filteredComments.add(comment);
                        }
                        break;
                    case "Author":
                        if(comment.getAuthor().contains(searchQuery))
                        {
                            filteredComments.add(comment);
                        }
                        break;
                }
            }
        }
        notifyDataSetChanged();
    }

    private void displayReplies(Comment comment, LinearLayout llReplies)
    {
        llReplies.removeAllViews();
        for(CommentReply commentReply : comment.getReplies().values())
        {
            TextView tvReply = new TextView(context);
            tvReply.setText(context.getString(R.string.comment_text, commentReply.getAuthor(), commentReply.getReply()));
            tvReply.setGravity(Gravity.CENTER_HORIZONTAL);
            tvReply.setOnClickListener(new CommentReplyOnClickListener(commentReply, llReplies));
            llReplies.addView(tvReply);
        }
    }

    private DoubleArgRunnable<Bitmap, ImageView> getImageOnSuccess = new DoubleArgRunnable<Bitmap, ImageView>()
    {
        @Override
        public void run(Bitmap bitmap, ImageView imageView)
        {
            imageView.setImageBitmap(bitmap);
        }
    };

    private DoubleArgRunnable<String, TextView> getImageOnError = new DoubleArgRunnable<String, TextView>()
    {
        @Override
        public void run(String text, TextView textView)
        {
            textView.setText(text);
            textView.setVisibility(View.VISIBLE);
        }
    };

    private class CommentReplyOnClickListener implements OnClickListener
    {

        private CommentReply commentReply;
        private LinearLayout llReplies;

        private CommentReplyOnClickListener(CommentReply commentReply, LinearLayout llReplies)
        {
            this.commentReply = commentReply;
            this.llReplies = llReplies;
        }

        @SuppressWarnings("deprecation")
        @Override
        public void onClick(View v)
        {
            int currentIndex = llReplies.indexOfChild(v);
            ColorDrawable colorDrawable = (ColorDrawable)v.getBackground();
            if(colorDrawable == null)
            {
                v.setBackgroundColor(context.getResources().getColor(R.color.blue));
                selectedCommentReply = commentReply;
            }
            else if(colorDrawable.getColor() == context.getResources().getColor(R.color.blue))
            {
                v.setBackgroundDrawable(null);
                selectedCommentReply = null;
            }
            for(int i = 0; i < llReplies.getChildCount(); i++)
            {
                if(i != currentIndex)
                {
                    llReplies.getChildAt(i).setBackgroundDrawable(null);
                }
            }
        }
    }

    private class CommentActionsOnClickListener implements OnClickListener
    {

        private Comment comment;
        private TextView tvReplies;

        private CommentActionsOnClickListener(Comment comment, TextView tvReplies)
        {
            this.comment = comment;
            this.tvReplies = tvReplies;
        }

        @Override
        public void onClick(View v)
        {
            switch(v.getId())
            {
                case R.id.btnAddReply:
                    addCommentReply();
                    break;
                case R.id.btnDeleteReply:
                    deleteCommentReply();
                    break;
            }
        }

        private void addCommentReply()
        {
            tvReplies.setError(null);
            PopupDialogs.showInputDialog(context, "Add Reply", "", "Post", "Cancel", onCommentReplyEntered);
        }

        private void deleteCommentReply()
        {
            tvReplies.setError(null);
            if(selectedCommentReply == null)
            {
                tvReplies.requestFocus();
                tvReplies.setError(context.getString(R.string.no_reply_selected));
            }
            else if(selectedCommentReply.getAuthor().equals(LoginData.getUsername()))
            {
                FirebaseData.getClassroomCommentReplyRef(selectedCommentReply.getCommentKey(), selectedCommentReply.getKey()).removeValue();
            }
            else
            {
                tvReplies.requestFocus();
                tvReplies.setError(context.getString(R.string.not_reply_author));
            }
        }

        private SingleArgRunnable<String> onCommentReplyEntered = new SingleArgRunnable<String>()
        {
            @Override
            public void run(String text)
            {
                if(text.isEmpty())
                {
                    Toast.makeText(context, R.string.no_text_entered, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    ClassroomData.postCommentReply(comment, text);
                }
            }
        };

    }

    private class ClassroomCommentComparator implements Comparator<Comment>
    {

        private String sortField;
        private String sortOrdering;

        public void setSortField(String sortField)
        {
            this.sortField = sortField;
        }

        public void setSortOrdering(String sortOrdering)
        {
            this.sortOrdering = sortOrdering;
        }

        @Override
        public int compare(Comment commentA, Comment commentB)
        {
            int compare = 0;
            switch(sortField)
            {
                case "Comment":
                    compare = commentA.getComment().compareTo(commentB.getComment());
                    break;
                case "Author":
                    compare = commentA.getAuthor().compareTo(commentB.getAuthor());
                    break;
                case "Date Created":
                    compare = commentA.getDateCreated().compareTo(commentB.getDateCreated());
                    break;
                case "# Replies":
                    compare = Integer.valueOf(commentA.getReplies().size()).compareTo(commentB.getReplies().size());
                    break;
            }
            return sortOrdering.equals("Ascending") ? compare : -1 * compare;
        }

    }

}