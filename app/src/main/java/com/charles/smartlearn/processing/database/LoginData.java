package com.charles.smartlearn.processing.database;

import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.database.students.User;
import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.firebase.client.*;
import com.firebase.client.Firebase.*;

public abstract class LoginData
{

    private static String username = "";
    private static String email = "";

    public static String getUsername()
    {
        return username;
    }

    public static String getEmail()
    {
        return email;
    }

    public static void setUsername(String username)
    {
        LoginData.username = username;
    }

    public static void setEmail(String email)
    {
        LoginData.email = email;
    }

    public static void login(final String email, final String password, final SingleArgRunnable<String> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.baseRef.authWithPassword(email, password, new AuthResultHandler()
        {
            @Override
            public void onAuthenticated(AuthData authData)
            {
                onSuccess.run(authData.getProviderData().get("email").toString());
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getUsernameFromEmail(final String email, final SingleArgRunnable<String> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.studentsRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    User user = child.getValue(User.class);
                    if(email.equals(user.getEmail()))
                    {
                        onSuccess.run(user.getUsername());
                        return;
                    }
                }
                onError.run(LearnApplication.getInstance().getString(R.string.not_student_account));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void resetPassword(final String email, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.baseRef.resetPassword(email, new ResultHandler()
        {
            @Override
            public void onSuccess()
            {
                onSuccess.run();
            }

            @Override
            public void onError(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

}