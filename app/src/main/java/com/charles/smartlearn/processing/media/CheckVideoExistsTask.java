package com.charles.smartlearn.processing.media;

import android.os.AsyncTask;
import android.widget.Toast;
import com.charles.smartlearn.R;
import com.charles.smartlearn.view.views.revision.notes.RevisionNotesActivity;
import java.io.IOException;

public class CheckVideoExistsTask extends AsyncTask<Void, Void, Boolean>
{

    private RevisionNotesActivity revisionNotesActivity;
    private String videoID;

    public CheckVideoExistsTask(RevisionNotesActivity revisionNotesActivity, String videoID)
    {
        this.revisionNotesActivity = revisionNotesActivity;
        this.videoID = videoID;
    }

    @Override
    protected Boolean doInBackground(Void... params)
    {
        Boolean exists;
        try
        {
            exists = VideoChecker.videoExists(videoID);
        }
        catch(IOException e)
        {
            exists = null;
        }
        return exists;
    }

    @Override
    protected void onPostExecute(Boolean success)
    {
        if(success == null)
        {
            Toast.makeText(revisionNotesActivity, R.string.network_error_checking_youtube_video_id, Toast.LENGTH_SHORT).show();
        }
        else if(success)
        {
            revisionNotesActivity.appendVideo(videoID);
        }
        else
        {
            Toast.makeText(revisionNotesActivity, R.string.youtube_video_id_invalid, Toast.LENGTH_SHORT).show();
        }
    }

}
