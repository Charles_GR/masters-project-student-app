package com.charles.smartlearn.processing.media;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import java.io.*;

public abstract class ImageParser
{

    public static String encodeImage(String path) throws FileNotFoundException
    {
        File file = new File(path);
        FileInputStream fileInputStream = new FileInputStream(file);
        Bitmap bitmap = BitmapFactory.decodeStream(fileInputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeImage(String encodedImage)
    {
        byte[] imageBytes = Base64.decode(encodedImage, 0);
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

}
