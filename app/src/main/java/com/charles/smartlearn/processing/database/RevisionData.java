package com.charles.smartlearn.processing.database;

import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.database.revision.notes.Comment;
import com.charles.smartlearn.model.database.revision.notes.Note;
import com.charles.smartlearn.model.database.revision.quizzes.Quiz;
import com.charles.smartlearn.model.runnable.*;
import com.charles.smartlearn.processing.media.ImageParser;
import com.firebase.client.*;
import com.firebase.client.Firebase.CompletionListener;
import java.io.*;
import java.util.*;

public abstract class RevisionData
{

    public static void getNotes(final ListRunnable<Note> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRevisionNotesRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Note> notes = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    notes.add(child.getValue(Note.class));
                }
                onSuccess.run(notes);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void postNote(String key, final String title, final String content, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        Firebase noteRef = (key == null) ? FirebaseData.getRevisionNotesRef().push() : FirebaseData.getRevisionNoteRef(key);
        noteRef.setValue(new Note(noteRef.getKey(), title, content), new CompletionListener()
        {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase)
            {
                if(firebaseError == null)
                {
                    onSuccess.run();
                }
                else
                {
                    onError.run(firebaseError.getMessage());
                }
            }
        });
    }

    public static void viewNote(final Activity activity, final String key, final TextView tvNoteTitle, final LinearLayout llNoteContent, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRevisionRef().addListenerForSingleValueEvent(new ValueEventListener()
        {
            @SuppressWarnings("unchecked")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                try
                {
                    Note note = dataSnapshot.child("notes").child(key).getValue(Note.class);
                    HashMap<String, String> images = dataSnapshot.child("images").getValue(HashMap.class);
                    note.createNoteViews(activity, images, tvNoteTitle, llNoteContent);
                }
                catch(OutOfMemoryError e)
                {
                    onError.run(activity.getString(R.string.not_enough_memory));
                    return;
                }
                onSuccess.run();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void deleteNote(final String key, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRevisionNotesRef().addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    if(child.getValue(Note.class).getKey().equals(key))
                    {
                        child.getRef().removeValue();
                        return;
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getNoteComments(String key, final ListRunnable<Comment> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRevisionNoteCommentsRef(key).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Comment> comments = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    comments.add(child.getValue(Comment.class));
                }
                onSuccess.run(comments);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void postNoteComment(final String key, final String text)
    {
        Firebase newCommentRef = FirebaseData.getRevisionNoteCommentsRef(key).push();
        Comment comment = new Comment(newCommentRef.getKey(), text);
        newCommentRef.setValue(comment);
    }

    @SuppressWarnings("unchecked")
    public static void getNoteImages(final HashMapRunnable<String, String> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRevisionImagesRef().addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                onSuccess.run(dataSnapshot.getValue(HashMap.class));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static String addImage(String path) throws FileNotFoundException
    {
        Firebase newImageRef = FirebaseData.getRevisionImagesRef().push();
        String encodedImage = ImageParser.encodeImage(path);
        newImageRef.setValue(encodedImage);
        LearnApplication.getInstance().getImages().put(newImageRef.getKey(), encodedImage);
        return newImageRef.getKey();
    }

    public static void getQuizzes(final ListRunnable<Quiz> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getRevisionQuizzesRef().addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                List<Quiz> quizzes = new ArrayList<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    quizzes.add(child.getValue(Quiz.class));
                }
                onSuccess.run(quizzes);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

}