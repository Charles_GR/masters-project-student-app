package com.charles.smartlearn.processing.database;

import com.charles.smartlearn.model.LearnApplication;
import com.firebase.client.Firebase;

public abstract class FirebaseData
{

    public static final Firebase baseRef = new Firebase("https://smart-apps.firebaseio.com/");
    public static final Firebase usersRef = baseRef.child("users");
    public static final Firebase studentsRef = usersRef.child("students");
    public static final Firebase coursesRef = baseRef.child("courses");

    public static Firebase getStudentRef(String student)
    {
        return studentsRef.child(student);
    }

    public static Firebase getCourseRef(String courseCode)
    {
        return coursesRef.child(courseCode);
    }

    public static Firebase getClassroomQuestionsRef()
    {
        return getCourseRef(LearnApplication.getInstance().getSelectedCourse().toString()).child("classroom").child("questions");
    }

    public static Firebase getClassroomQuestionRef(String key)
    {
        return getClassroomQuestionsRef().child(key);
    }

    public static Firebase getClassroomCommentsRef()
    {
        return getCourseRef(LearnApplication.getInstance().getSelectedCourse().toString()).child("classroom").child("comments");
    }

    public static Firebase getClassroomCommentRepliesRef(String key)
    {
        return getClassroomCommentsRef().child(key).child("replies");
    }

    public static Firebase getClassroomCommentReplyRef(String commentKey, String replyKey)
    {
        return getClassroomCommentRepliesRef(commentKey).child(replyKey);
    }

    public static Firebase getClassroomImagesRef()
    {
        return getCourseRef(LearnApplication.getInstance().getSelectedCourse().toString()).child("classroom").child("images");
    }

    public static Firebase getClassroomImageRef(String key)
    {
        return getClassroomImagesRef().child(key);
    }

    public static Firebase getRevisionRef()
    {
        return getCourseRef(LearnApplication.getInstance().getSelectedCourse().toString()).child("revision");
    }

    public static Firebase getRevisionNotesRef()
    {
        return getRevisionRef().child("notes");
    }

    public static Firebase getRevisionNoteRef(String key)
    {
        return getRevisionNotesRef().child(key);
    }

    public static Firebase getRevisionNoteCommentsRef(String key)
    {
        return getRevisionNoteRef(key).child("comments");
    }

    public static Firebase getRevisionImagesRef()
    {
        return getRevisionRef().child("images");
    }

    public static Firebase getRevisionQuizzesRef()
    {
        return getRevisionRef().child("quizzes");
    }

    public static Firebase getRevisionQuizResponseRef(String quizKey, String student)
    {
        return getRevisionQuizzesRef().child(quizKey).child("responses").child(student);
    }

    public static Firebase getPerformancesRef(String courseCode)
    {
        return getCourseRef(courseCode).child("performances");
    }

    public static Firebase getPerformanceRef(String courseCode, String username)
    {
        return getPerformancesRef(courseCode).child(username);
    }

}