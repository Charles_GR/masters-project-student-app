package com.charles.smartlearn.processing.database;

import com.charles.smartlearn.model.database.students.Course;
import com.charles.smartlearn.model.runnable.*;
import com.firebase.client.*;
import java.util.*;

public abstract class CourseData
{

    public static void getCourses(final ListRunnable<Course> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.coursesRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Set<Course> courses = new TreeSet<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    courses.add(child.child("info").getValue(Course.class));
                }
                onSuccess.run(new ArrayList<>(courses));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getStudentCourses(final ListRunnable <Course> onSuccess, final SingleArgRunnable<FirebaseError> onError)
    {
        FirebaseData.coursesRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                Set<Course> studentCourses = new TreeSet<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    Course course = child.child("info").getValue(Course.class);
                    if(course.getStudents().containsValue(LoginData.getUsername()))
                    {
                        studentCourses.add(course);
                    }
                }
                onSuccess.run(new ArrayList<>(studentCourses));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError);
            }
        });
    }

}