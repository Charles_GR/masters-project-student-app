package com.charles.smartlearn.processing.media;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class VideoChecker
{

    public static final String YOUTUBE_API_KEY = "AIzaSyAzAiq3SXtebbaRAzTumIOX_p3CCrE0Tg8";

    public static boolean videoExists(String videoID) throws IOException
    {
        String videoDataUrl = "https://www.googleapis.com/youtube/v3/videos?id=" + videoID + "&key=" + YOUTUBE_API_KEY + "&part=snippet";
        String videoDataSource = fetchUrlSource(videoDataUrl);
        JsonObject jsonObject = new JsonParser().parse(videoDataSource).getAsJsonObject();
        return jsonObject.get("pageInfo").getAsJsonObject().get("totalResults").getAsInt() > 0;
    }

    private static String fetchUrlSource(String address) throws IOException
    {
        StringBuilder source = new StringBuilder();
        URL url = new URL(address);
        HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.0; rv:25.0) Gecko/20100101 Firefox/25.0");
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setReadTimeout(10000);
        InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String line = bufferedReader.readLine();
        while(line != null)
        {
            source.append(line);
            line = bufferedReader.readLine();
        }
        bufferedReader.close();
        httpURLConnection.disconnect();
        return source.toString();
    }

}

