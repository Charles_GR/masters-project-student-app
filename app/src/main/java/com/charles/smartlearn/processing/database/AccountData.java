package com.charles.smartlearn.processing.database;

import com.charles.smartlearn.model.runnable.SingleArgRunnable;
import com.firebase.client.*;
import com.firebase.client.Firebase.CompletionListener;
import com.firebase.client.Firebase.ResultHandler;

public abstract class AccountData
{

    public static void changeEmail(final String currentEmail, final String password, final String newEmail, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.studentsRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                FirebaseData.baseRef.changeEmail(currentEmail, password, newEmail, new ResultHandler()
                {
                    @Override
                    public void onSuccess()
                    {
                        updateEmailLink(newEmail, onSuccess, onError);
                    }

                    @Override
                    public void onError(FirebaseError firebaseError)
                    {
                        onError.run(firebaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void updateEmailLink(final String email, final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getStudentRef(LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                FirebaseData.getStudentRef(LoginData.getUsername()).child("email").setValue(email, new CompletionListener()
                {
                    @Override
                    public void onComplete(FirebaseError firebaseError, Firebase firebase)
                    {
                        if(firebaseError == null)
                        {
                            LoginData.setEmail(email);
                            onSuccess.run();
                        }
                        else
                        {
                            onError.run(firebaseError.getMessage());
                        }
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void changePassword(final String email, final String currentPassword, final String newPassword,
                               final Runnable onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.baseRef.changePassword(email, currentPassword, newPassword, new ResultHandler()
        {
            @Override
            public void onSuccess()
            {
                onSuccess.run();
            }

            @Override
            public void onError(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

}
