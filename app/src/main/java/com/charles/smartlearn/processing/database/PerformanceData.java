package com.charles.smartlearn.processing.database;

import android.widget.TextView;
import com.charles.smartlearn.R;
import com.charles.smartlearn.model.LearnApplication;
import com.charles.smartlearn.model.database.performance.Performance;
import com.charles.smartlearn.model.database.performance.Score;
import com.charles.smartlearn.model.database.performance.Score.ContentType;
import com.charles.smartlearn.model.runnable.*;
import com.firebase.client.*;
import java.util.*;

public abstract class PerformanceData
{

    public static void getCoursePerformances(final String courseCode, final String timePeriod, final HashMapRunnable<String, Performance> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getPerformancesRef(courseCode).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                HashMap<String, Performance> performances = new HashMap<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    if(child.getKey().equals(LoginData.getUsername()) || child.child("shared").getValue(Boolean.class))
                    {
                        Performance performance = child.getValue(Performance.class);
                        performance.filterByTime(timePeriod);
                        if(performance.calcPointsPossible() > 0)
                        {
                            performances.put(child.getKey(), performance);
                        }
                    }
                }
                onSuccess.run(performances);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getStudentPerformances(final String timePeriod, final HashMapRunnable<String, Performance> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.coursesRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                HashMap<String, Performance> performances = new HashMap<>();
                for(DataSnapshot child : dataSnapshot.getChildren())
                {
                    String courseCode = child.child("info").child("code").getValue(String.class);
                    Performance performance = child.child("performances").child(LoginData.getUsername()).getValue(Performance.class);
                    if(performance != null)
                    {
                        performance.filterByTime(timePeriod);
                        if(performance.calcPointsPossible() > 0)
                        {
                            performances.put(courseCode, performance);
                        }
                    }
                }
                onSuccess.run(performances);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void getQuizPerformance(final String quizKey, final TextView tvYourScoreValue, final DoubleArgRunnable<Score, TextView> onSuccess, final DoubleArgRunnable<String, TextView> onError)
    {
        FirebaseData.getPerformanceRef(LearnApplication.getInstance().getSelectedCourse().toString(), LoginData.getUsername()).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                for(DataSnapshot child : dataSnapshot.child("scores").getChildren())
                {
                    Score score = child.getValue(Score.class);
                    if(quizKey.equals(score.getContentKey()))
                    {
                        onSuccess.run(score, tvYourScoreValue);
                        return;
                    }
                }
                onSuccess.run(null, tvYourScoreValue);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(LearnApplication.getInstance().getString(R.string.not_yet_available), tvYourScoreValue);
            }
        });
    }

    public static void updatePerformance(final ContentType contentType, final String contentKey, final int questionCount, final int pointsAwarded, final int pointsPossible)
    {
        FirebaseData.getPerformanceRef(LearnApplication.getInstance().getSelectedCourse().toString(), LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(!dataSnapshot.exists())
                {
                    dataSnapshot.child("shared").getRef().setValue(false);
                }
                if(contentType.equals(ContentType.REVISION_QUIZ_TYPE))
                {
                    for(DataSnapshot child : dataSnapshot.child("scores").getChildren())
                    {
                        Score score = child.getValue(Score.class);
                        if(contentKey.equals(score.getContentKey()))
                        {
                            child.getRef().setValue(new Score(contentType, contentKey, questionCount, pointsAwarded, pointsPossible));
                            return;
                        }
                    }
                }
                dataSnapshot.child("scores").getRef().push().setValue(new Score(contentType, contentKey, questionCount, pointsAwarded, pointsPossible));
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {

            }
        });
    }

    public static void getPerformanceDataSharedValue(final String courseCode, final SingleArgRunnable<Boolean> onSuccess, final SingleArgRunnable<String> onError)
    {
        FirebaseData.getPerformanceRef(courseCode, LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(dataSnapshot.exists())
                {
                    onSuccess.run(dataSnapshot.child("shared").getValue(Boolean.class));
                }
                else
                {
                    onSuccess.run(false);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {
                onError.run(firebaseError.getMessage());
            }
        });
    }

    public static void setPerformanceDataSharedValue(final String courseCode, final boolean shared, final Runnable onAbort)
    {
        FirebaseData.getPerformanceRef(courseCode, LoginData.getUsername()).addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if(!dataSnapshot.exists())
                {
                    onAbort.run();
                    return;
                }
                dataSnapshot.child("shared").getRef().setValue(shared);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError)
            {

            }
        });
    }

}